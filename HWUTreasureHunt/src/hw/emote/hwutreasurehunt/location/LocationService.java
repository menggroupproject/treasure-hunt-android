package hw.emote.hwutreasurehunt.location;

import hw.emote.hwutreasurehunt.HWUGeneral;
import hw.emote.hwutreasurehunt.LogManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import android.app.Service;
import android.content.Intent;
import android.hardware.GeomagneticField;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;


/**
 * @author Mei Yii Lim
 *
 * This is the class that provides location information through GPS
 * 
 * It implements the LocationListener to get GPS reading 
 */
public class LocationService extends Service implements
		LocationListener,
		ConnectionCallbacks,
		OnConnectionFailedListener {
		
	private static final String TAG = "LocationService";	
	
	public static final String BROADCAST_LOCATION = "hw.emote.hwutreasurehunt.locationservice";
	public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String DECLINATION = "declination";
    public static final String GPS_LOG_FILE = "gpsFile";
    
	private static final long UPDATE_INTERVAL = 10000; // in Milliseconds
	private static final long MINIMUM_UPDATE_INTERVAL = UPDATE_INTERVAL/2; // in Milliseconds
	private static final long DISPLACEMENT = 5; // in Metres
	
    /**
     * Provides the entry point to Google Play services.
     */
    private GoogleApiClient mGoogleApiClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private LocationRequest mLocationRequest;

    /**
     * Represents a geographical location.
     */
    private Location mCurrentLocation;

    /**
     * External Storage Log 
     */
 	private LogManager mEALogManager;
 	private String mLogFileName;
 	
    /**
     * Intent to broadcast the location changes
     */
    private Intent mIntent;
	
	//private List<Geofence> geofenceList;
	
	@Override
	public void onCreate() 
	{
		super.onCreate();
	
		// intent to broadcast the location changes
    	mIntent = new Intent(BROADCAST_LOCATION);   
    	
		// Kick off the process of building a GoogleApiClient and requesting the LocationService API.
    	// First we need to check availability of play services
        if (checkPlayServices()) { 
        	buildGoogleApiClient();
        }
				
	}
	
	/**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) != ConnectionResult.SUCCESS) {
        	//Toast.makeText(getApplicationContext(), "Google Play Service unavailable!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    
	/**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    private synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }
    
    /**
     * Sets up the location request. 
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is inexact. 
        // There may not be updates at all if no location sources are available, or it may be slower than requested. 
        // It may also be faster than requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL);

        // Sets the fastest rate for active location updates. This interval is exact, the
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(MINIMUM_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Sets the smallest distance for updates
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }
    
    /**
     * Requests location updates from the FusedLocationApi.
     */
    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    
    /**
     * Removes location updates from the FusedLocationApi.
     */
    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {     
    	 // Log Manager
 	 	mEALogManager = new LogManager();
        mLogFileName = intent.getStringExtra(HWUGeneral.LOG_FILE) + ".gpx";
        if (mEALogManager.checkStorageStatus()) {
 	 		mEALogManager.createGPSFile(mLogFileName);
 	 	}
    	mGoogleApiClient.connect();
    	
    	// We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");
        //Toast.makeText(getApplicationContext(), "Location Service connected to GoogleApiClient!", Toast.LENGTH_LONG).show();
        // start the location updates
        startLocationUpdates();
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        float declination = calculateDeclination();
        // log the current location
        writeToGPSLog(mCurrentLocation);
        //Log.i(TAG, "Current Location: " + mCurrentLocation.toString()); 
        
        // update the intent
        mIntent.putExtra(LATITUDE, mCurrentLocation.getLatitude());
        mIntent.putExtra(LONGITUDE, mCurrentLocation.getLongitude());
        mIntent.putExtra(DECLINATION, declination);
        sendBroadcast(mIntent);
    }
    
    private float calculateDeclination() {
    	return new GeomagneticField((float) mCurrentLocation.getLatitude(),
 	            (float) mCurrentLocation.getLongitude(), (float) mCurrentLocation.getAltitude(),
 	            mCurrentLocation.getTime()).getDeclination();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onDestroy() {
		Log.d(TAG, "Service is destroyed");
		stopLocationUpdates();
		mGoogleApiClient.disconnect(); 
		mEALogManager.terminateGPSFile(mLogFileName);
		super.onDestroy();
	}
	
	/**
	 * Logging the GPS data
	 * @param location
	 */
	private void writeToGPSLog(Location location) {
		if (mEALogManager.checkStorageStatus()) {
			mEALogManager.writeToGPSFile(mLogFileName, location);
		}
	}
}

/*public class HWULocationService extends Service implements LocationListener {
 	
    private LocationManager mLocationManager;
    private Location mLocation;
    private long mPreviousTime;
 	
    public static final String BROADCAST_DECLINATION = "hw.emote.eatreasurehunt.ealocationservice";
    public static final String DECLINATION = "declination";
    public static final String ELAPSED_TIME = "elapsedTime";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String GPS_LOG_FILE = "gpsFile";
 
    // External Storage Log 
 	private HWULogManager mEALogManager;
 	private String mLogFileName;
 	private boolean mLogFileCreated = false;
 	
    /**
     * Intent to broadcast the declination when location changes
     *
    private Intent mIntent;
    
    @Override
    public void onCreate() {
    
    	// intent to broadcast the declination
    	mIntent = new Intent(BROADCAST_DECLINATION);        
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mPreviousTime = SystemClock.elapsedRealtime();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {        
        for (final String provider : mLocationManager.getProviders(true)) {
            if (LocationManager.GPS_PROVIDER.equals(provider)
                    || LocationManager.PASSIVE_PROVIDER.equals(provider)
                    || LocationManager.NETWORK_PROVIDER.equals(provider)) {
                if (mLocation == null) {
                    mLocation = mLocationManager.getLastKnownLocation(provider);
                }
                // update location only every 10s and only when the distance has changed by 10 metres
                //mLocationManager.requestLocationUpdates(provider, 10000, 10.0f, this);
                mLocationManager.requestLocationUpdates(provider, 5000, 5.0f, this);
            }
        }
 	 	
        // Log Manager
 	 	mEALogManager = new HWULogManager();
        mLogFileName = intent.getStringExtra(HWUGeneral.LOG_FILE) + ".gpx";
        if (mEALogManager.checkStorageStatus()) {
 	 		mEALogManager.createGPSFile(mLogFileName);
 	 	}
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    @Override
	public IBinder onBind(Intent intent) {
		return null;
	}
    
	@Override
	public void onDestroy() {
	    //Toast.makeText(this, "Location service destroyed", Toast.LENGTH_SHORT).show();    
        mLocationManager.removeUpdates(this);
        mEALogManager.terminateGPSFile(mLogFileName);
	}
	
 	/**
     * @return current location
     *
    public Location getCurrentLocation() {
        return mLocation;
    }
		
	public void onLocationChanged(Location location) {		
        mLocation = location;
        writeToGPSLog(mLocation);
        
        long elapsedTime = SystemClock.elapsedRealtime() - mPreviousTime;
        mIntent.putExtra(ELAPSED_TIME, elapsedTime); 
        //Log.d(getClass().getSimpleName(), "location " + mLocation);
        //broadcast the new location
        mIntent.putExtra(LATITUDE, mLocation.getLatitude());
        mIntent.putExtra(LONGITUDE, mLocation.getLongitude());		
        sendBroadcast(mIntent);
	}
	
	
	
	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
	}
	
	/**
	 * Logging the GPS data
	 * @param location
	 *
	private void writeToGPSLog(Location location) {
		if (mEALogManager.checkStorageStatus()) {
			mEALogManager.writeToGPSFile(mLogFileName, location);
		}
	}
}*/
