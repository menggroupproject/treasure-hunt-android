package hw.emote.hwutreasurehunt.location;

import android.location.Location;
import android.util.Log;

public class UserTracking {
	
	private static final String TAG = "UserTracking";
	private Location mCurrentLocation;
	private Location mPreviousLocation;
	private Location mNextDestination;
	
	public UserTracking() {
		mCurrentLocation = new Location("Current");
    	mCurrentLocation.setLatitude(0.0);
    	mCurrentLocation.setLongitude(0.0);
    	mPreviousLocation = new Location("Previous");
    	mPreviousLocation.setLatitude(0.0);
    	mPreviousLocation.setLongitude(0.0);
		mNextDestination = new Location("Destination");
		mNextDestination.setLatitude(0.0);
    	mNextDestination.setLongitude(0.0);
	}
	
	/**
	 * sets the current location
	 */
	public void setCurrentLocation(double latitude, double longitude) {
		mPreviousLocation.set(mCurrentLocation);
		mCurrentLocation.setLatitude(latitude);
		mCurrentLocation.setLongitude(longitude);
		//Log.d(TAG,"previous latitude: " + mPreviousLocation.getLatitude() + " longitude: " + mPreviousLocation.getLongitude());
		//Log.d(TAG,"current latitude: " + mCurrentLocation.getLatitude() + " longitude: " + mCurrentLocation.getLongitude());
	}
	
	/**
	 * sets the current location
	 */
	public void setCurrentLocation(Location currentLocation) {
		mPreviousLocation.set(mCurrentLocation);
		mCurrentLocation.set(currentLocation);
	}
	
	public Location getCurrentLocation() {
		return mCurrentLocation;
	}
	
	/**
	 * sets the next destination
	 */
	public void setNextDestination(double latitude, double longitude) {
		mNextDestination.setLatitude(latitude);
		mNextDestination.setLongitude(longitude);
	}
	
	/**
	 * @return the next destination
	 */
	public Location getNextDestination() {
		return mNextDestination;
	}
	
	 /**
     * @return user bearing
     */
    public double getUserBearing() {
    	return mPreviousLocation.bearingTo(mCurrentLocation);
    	//Log.d(TAG,"bearingTo: " + (mPreviousLocation.bearingTo(mCurrentLocation) + 180) % 360);
    	//Log.d(TAG,"getBearing: " + getBearing(mPreviousLocation, mCurrentLocation));
    }
    
    /**
     * @return bearing from the current location to destination
     */
    public double getDestinationBearing() {
    	return mCurrentLocation.bearingTo(mNextDestination);
    	//Log.d(TAG,"bearingTo: " + (mCurrentLocation.bearingTo(mNextDestination) + 180) % 360);
    	//Log.d(TAG,"getBearing: " + getBearing(mCurrentLocation, mNextDestination));
    }
    
    private double getBearing(Location start, Location end) {  
    	// convert the coordinates to radian
    	double phi1 = degToRad(start.getLatitude());
    	double phi2 = degToRad(end.getLatitude());
    	double lam1 = degToRad(start.getLongitude());
    	double lam2 = degToRad(end.getLongitude());    	
    		
    	// calculate the bearing
    	double bearing = radToDeg(Math.atan2(Math.sin(lam2-lam1)*Math.cos(phi2),
    			Math.cos(phi1)*Math.sin(phi2) - Math.sin(phi1)*Math.cos(phi2)*Math.cos(lam2-lam1)));
    	// calculate the initial bearing 
    	return (bearing + 360.0) % 360;
    	//return bearing;
    }
    
    private double degToRad(double deg) {
    	return deg * (Math.PI/180.0);
    }
    
    private double radToDeg(double rad) {
    	return rad * (180/Math.PI);
    }    
    
    /**
     * @return distance between 2 points
     */
    public float getDistance(Location startPoint, Location endPoint) {
    	return startPoint.distanceTo(endPoint);
    }
    
    /**
     * @return distance from current point to the next destination
     */
    public float getDistanceToDestination() {
    	return mCurrentLocation.distanceTo(mNextDestination);
    }
    
    /**
     * @return speed of user movement
     */
    public float getUserSpeed() {
    	return mCurrentLocation.getSpeed();
    }

}
