package hw.emote.hwutreasurehunt.location;

import hw.emote.eatreasurehunt.R;
import hw.emote.routeparser.Point;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;

public class GeofenceHandler implements 
	ConnectionCallbacks,
	ResultCallback<Status>, 
	LocationListener,
	OnConnectionFailedListener {
	
	private static final String TAG = "GeofenceHandler";
	public static final float GEOFENCE_RADIUS_IN_METERS = 25.0f;
	public static final long MILLISECONDS = 1000;    
    public static final long SECONDS_IN_MINUTE = 60;    
    public static final long MINUTES_IN_HOUR = 60;    
    public static final long EXPIRATION_DURATION = 10*MINUTES_IN_HOUR*SECONDS_IN_MINUTE*MILLISECONDS;    
    public static final long TIME_BETWEEN_UPDATES = 5*MILLISECONDS; // in Milliseconds
	
	 // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private PendingIntent mGeofencePendingIntent;
    private ArrayList<Geofence> mGeofenceList;
    private List<String> mGeofenceIds;
    private List<String> mGeofenceIdsToRemove;
    private final Activity mActivity;
    private boolean mGeofencesAdding;
    
    public GeofenceHandler(Activity activity) {
    	mActivity = activity;
    	// Initially set the PendingIntent used in addGeofences() and removeGeofences() to null.
        mGeofencePendingIntent = null;
        // Empty list for storing geofences.
        mGeofenceList = new ArrayList<Geofence>();
        mGeofenceIds = new ArrayList<String>();
        mGeofenceIdsToRemove = new ArrayList<String>();
        mGeofencesAdding = false;
              
    	// Kick off the process of building a GoogleApiClient and requesting the LocationService API.
    	// First we need to check availability of play services
        if (checkPlayServices()) { 
        	//Toast.makeText(activity.getApplicationContext(), "Geofence Google Play Service available!", Toast.LENGTH_LONG).show();
        	Log.i(TAG, "Google Play Service available!");
        	buildGoogleApiClient();
        	mGoogleApiClient.connect();
        }
    }
    
    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(mActivity.getApplicationContext()) != ConnectionResult.SUCCESS) {
        	//Toast.makeText(getApplicationContext(), "Google Play Service unavailble!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    
    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        //mGoogleApiClient.connect();
    }
	
	/**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        //Toast.makeText(mActivity, "GeofenceRequest " + mGeofenceList.toString(), Toast.LENGTH_SHORT).show();
        Log.i(TAG, "GeofenceRequest " + mGeofenceList.toString());
        
        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(mGeofenceList);

        // Return a GeofencingRequest.
        return builder.build();
    }
    
    /**
  	 * Create geofences from a location
  	 */  	
  	public void populateGeofenceList(ArrayList<Point> points) {
  		mGeofenceIdsToRemove.clear();
  		mGeofenceList.clear();
  		mGeofenceIds.clear();
        for (int i = 0; i < points.size(); i++) {
        	Point point = points.get(i);
        	mGeofenceIds.add(point.getName());
        	//Log.i(TAG, "Geofence Point: " + point.toString());
            mGeofenceList.add(new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(point.getName())

                    // Set the circular region of this geofence.
                    .setCircularRegion(
                            point.getLatitude(),
                            point.getLongitude(),
                            GEOFENCE_RADIUS_IN_METERS
                    )

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER) 

                    // Create the geofence.
                    .build());
        }
    } 	
  	
  	 /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */
    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
        	//Toast.makeText(mActivity, "Pending intent: " + mGeofencePendingIntent.toString(), Toast.LENGTH_LONG).show();
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(mActivity, GeofenceService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        return PendingIntent.getService(mActivity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
    
    private void logSecurityException(SecurityException securityException) {
        Log.e(TAG, "Invalid location permission. " +
                "You need to use ACCESS_FINE_LOCATION with geofences", securityException);
    }
    
    public void addGeofence() {
    	if (isGoogleApiClientConnected()){    	
	    	 try {
	    		 mGeofencesAdding = true;
	             LocationServices.GeofencingApi.addGeofences(
	                     mGoogleApiClient,
	                     // The GeofenceRequest object.
	                     getGeofencingRequest(),
	                     // A pending intent that that is reused when calling removeGeofences(). 
	                     // This pending intent is used to generate an intent when a matched geofence transition is observed.
	                     getGeofencePendingIntent()
	             ).setResultCallback(this); // Result processed in onResult()
	         } catch (SecurityException securityException) {
	             // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
	             logSecurityException(securityException);
	         }
    	}
    }
    
    public void removesGeofence() {
    	if (isGoogleApiClientConnected()){    	
	    	try {
	            // Remove geofences.
	    		mGeofencesAdding = false;
	    		mGeofenceIdsToRemove = mGeofenceIds;
	            LocationServices.GeofencingApi.removeGeofences(
	                    mGoogleApiClient,
	                    // This is the same pending intent that was used in addGeofences().
	                    getGeofencePendingIntent()
	            ).setResultCallback(this); // Result processed in onResult().
	        } catch (SecurityException securityException) {
	            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
	            logSecurityException(securityException);
	        }
    	}
    }
    
    public void removesGeofence(List<String> geofenceIds) {
    	if (isGoogleApiClientConnected()){    	
	    	try {
	            // Remove geofences.
	    		mGeofencesAdding = false;
	    		mGeofenceIdsToRemove = geofenceIds;
	    		if (geofenceIds != null) {
		            LocationServices.GeofencingApi.removeGeofences(
		                    mGoogleApiClient,
		                    // This is the list of geofences to be removed
		                    geofenceIds
		            ).setResultCallback(this); // Result processed in onResult().
	    		}
	        } catch (SecurityException securityException) {
	            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
	            logSecurityException(securityException);
	        }
    	}
    }
    
    /**
     * Runs when the result of calling addGeofences() and removeGeofences() becomes available.
     * Either method can complete successfully or with an error.
     *
     * Since this activity implements the {@link ResultCallback} interface, we are required to
     * define this method.
     *
     * @param status The Status returned through a PendingIntent when addGeofences() or
     *               removeGeofences() get called.
     */
    public void onResult(Status status) {
        if (status.isSuccess()) {            
        	// Update state           
            /*Toast.makeText(mActivity, mActivity.getString(mGeofencesAdding ? R.string.geofences_added : R.string.geofences_removed) 
            		+ " " + (mGeofencesAdding ? mGeofenceIds : mGeofenceIdsToRemove),  
            		Toast.LENGTH_SHORT).show();*/
            Log.i(TAG, mActivity.getString(mGeofencesAdding ? R.string.geofences_added : R.string.geofences_removed) 
            		+ " " + (mGeofencesAdding ? mGeofenceIds : mGeofenceIdsToRemove));
        } else {
        	//Toast.makeText(mActivity, "Geofence Error", Toast.LENGTH_LONG).show();
        	Log.e(TAG, "Geofence add/remove error");
        }
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		
	}
	
	private boolean isGoogleApiClientConnected(){
		if (!mGoogleApiClient.isConnected()) {
            return false;
		}
		return true;
	}
}
