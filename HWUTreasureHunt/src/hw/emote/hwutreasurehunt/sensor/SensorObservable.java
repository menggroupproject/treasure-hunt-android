package hw.emote.hwutreasurehunt.sensor;

import java.util.Observable;

import android.app.Activity;
import org.json.JSONException;

//import hw.emote.hwutreasurehunt.sensor.SensorValue.State;
//import static hw.emote.hwutreasurehunt.sensor.SensorValue.State.*;

/**
 * Wrapper class for handling sensor information from either
 * ANT+ bands or microsoft bands. The wrapper class is used
 * so a single common interface can be integrated into the
 * HWUTreasureHuntActivity class for readability.
*/
public abstract class SensorObservable extends Observable {

    /*                setChanged();
                notifyObservers(new SensorValue(
                    SensorValue.Type.HEART_RATE,
                    new Object[] { new Float( computedHeartRate ), dataState },
                    new State[]  { ZERO_DETECTED }
                ));*/

    /**
     * Called to reset the band.
     * Was used in ANT+ might not be relevant to MS band.
    */
    public abstract void handleReset( Activity activity );

    /**
     * Called to stop the band service.
     * Was used in ANT+ but might be relvant to MS band.
    */
    public abstract void stop();
}