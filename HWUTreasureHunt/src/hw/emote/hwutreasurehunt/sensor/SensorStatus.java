package hw.emote.hwutreasurehunt.sensor;

public class SensorStatus {

    //Generic state of the band we are reading
    public enum Status {
        SUCCESS,
        ERROR,
        MESSAGE
    }

    private String message = "";
    private Status status  = Status.SUCCESS;

    public SensorStatus( Status status, String message ) {
        this.status  = status;
        this.message = message;
    }

    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}