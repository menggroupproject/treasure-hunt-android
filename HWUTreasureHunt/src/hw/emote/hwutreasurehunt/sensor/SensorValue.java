package hw.emote.hwutreasurehunt.sensor;

public class SensorValue {

    //Generic type of the sesnsor we are reading
    public enum Type {
        HEART_RATE,
        DISTANCE,
        PEDOMETER,
        SKIN_TEMPERATURE
    }

    private Object[] values;
    private Type     type   = Type.HEART_RATE;

    public SensorValue( Type type, Object[] values ) {
        this.type   = type;
        this.values = values;
    }

    public Type getType() {
        return type;
    }

    public Object[] getValues() {
        return values;
    }
}