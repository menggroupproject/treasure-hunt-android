package hw.emote.hwutreasurehunt.affect;

public class Valence {
	
	public final static String POSITIVE = "positive"; 
	public final static String NEGATIVE = "negative";
	public final static String NEUTRAL = "neutral";
	
	private int mCorrect;
	private int mTotalQ;
	private int mInterventions; 
	private int mNIPoints; // 5 points for every step completed without intervention
	private int mUnlocked;
	private int mTotalTreasures;
	private String mValence;	
	
	public Valence() {
		setCorrect(0);
		setTotalQ(0);
		setInterventions(0);
		setNIPoints(0);
		setUnlocked(0);
		setTotalTreasures(0);
		setValence("");
	}

	public String getValence() {
		if (getScore() > 0.7) {
			mValence = POSITIVE;
		} else if (getScore() < 0.3) {
			mValence = NEGATIVE;
		} else {
			mValence = NEUTRAL;
		}
		return mValence;
	}

	public void setValence(String valence) {
		this.mValence = valence;
	}

	public float getScore() {
		float qaScore = 0; // Q & A score
		float iScore = 0; // intervention score
		float tScore = 0; // treasure score
		float avgScore;
		
		// avoid division by 0
		if (mTotalQ > 0) {
			qaScore = mCorrect/mTotalQ;
		}
		if (mNIPoints > 0) {
			iScore = (mNIPoints - mInterventions)/mNIPoints;
		}
		if (mTotalTreasures > 0) {
			tScore = mUnlocked/mTotalTreasures;
		}			
		avgScore = ((qaScore + iScore + tScore)/3) + 0.5f;	
		return avgScore;
	}

	public int getCorrect() {
		return mCorrect;
	}

	public void setCorrect(int correct) {
		this.mCorrect = correct;
	}

	public int getTotalQ() {
		return mTotalQ;
	}

	public void setTotalQ(int totalQ) {
		this.mTotalQ = totalQ;
	}
	
	public int getInterventions() {
		return mInterventions;
	}

	public void setInterventions(int interventions) {
		this.mInterventions = interventions;
	}

	public int getNIPoints() {
		return mNIPoints;
	}

	public void setNIPoints(int niPoints) {
		this.mNIPoints = niPoints;
	}
	
	public int getUnlocked() {
		return mUnlocked;
	}

	public void setUnlocked(int unlocked) {
		this.mUnlocked = unlocked;
	}

	public int getTotalTreasures() {
		return mTotalTreasures;
	}

	public void setTotalTreasures(int totalTreasures) {
		this.mTotalTreasures = totalTreasures;
	}
	
	public void increaseCorrect() {
		mCorrect++;
	}
	
	public void increaseTotalQ() {
		mTotalQ++;
	}

	public void increaseIntervention() {
		mInterventions++;
	}

	public void increaseUnlocked() {
		mUnlocked++;
	}

	public void increaseTotalTreasures() {
		mTotalTreasures++;
	}
}
