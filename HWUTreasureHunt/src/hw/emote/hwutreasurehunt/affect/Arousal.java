package hw.emote.hwutreasurehunt.affect;

public class Arousal {
	public final static String HIGH = "high";
	public final static String MED = "med"; 
	public final static String LOW = "low";
	
	private String mArousal;
	private float mBaseline;
	private float mHeartRate;
	private float mMRange;
	private float mSRange;

	public Arousal() {
		mArousal = "";
		mBaseline = 0.0f;
		mHeartRate = 0.0f;
		mMRange = 0.0f;
		mSRange = 0.0f;
	}
	
	public String getMovingArousal() {
		float rate = mHeartRate - mBaseline;
		if (rate > mMRange) {
			mArousal = HIGH;
		} else if (rate < (mMRange/2)) {
			mArousal = LOW;
		} else {
			mArousal = MED;
		}
		return mArousal;
	}

	public String getStaticArousal() {
		float rate = mHeartRate - mBaseline;
		if (rate > mSRange) {
			mArousal = HIGH;
		} else if (rate < (mSRange/2)) {
			mArousal = LOW;
		} else {
			mArousal = MED;
		}
		return mArousal;
	}
	
	public void setArousal(String arousal) {
		this.mArousal = arousal;
	}

	public float getBaseline() {
		return mBaseline;
	}

	public void setBaseline(float baseline) {
		this.mBaseline = baseline;
	}

	public float getHeartRate() {
		return mHeartRate;
	}

	public void setHeartRate(float heartRate) {
		this.mHeartRate = heartRate;
	}

	public float getMRange() {
		return mMRange;
	}

	public void setMRange(float mRange) {
		this.mMRange = mRange;
	}

	public float getSRange() {
		return mSRange;
	}

	public void setSRange(float mSRange) {
		this.mSRange = mSRange;
	}
}
