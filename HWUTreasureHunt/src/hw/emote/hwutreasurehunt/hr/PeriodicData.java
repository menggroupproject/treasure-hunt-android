package hw.emote.hwutreasurehunt.hr;

/**
 * Created by Peter on 03/03/2016.
 */
public class PeriodicData {

    private int heartRate;
    private int ambientLight;
    private String uv;
    private float speed;
    private double skin;


    public PeriodicData() {
    }

    public PeriodicData(int heartRate, int ambientLight, String uv, float speed) {
        this.heartRate = heartRate;
        this.ambientLight = ambientLight;
        this.uv = uv;
        this.speed = speed;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public int getAmbientLight() {
        return ambientLight;
    }

    public void setAmbientLight(int ambientLight) {
        this.ambientLight = ambientLight;
    }

    public String getUv() {
        return uv;
    }

    public void setUv(String uv) {
        this.uv = uv;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public double getSkin() {
        return skin;
    }

    public void setSkin(double skin) {
        this.skin = skin;
    }
}
