package hw.emote.hwutreasurehunt.hr;

import java.math.BigDecimal;
import java.util.EnumSet;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.os.IBinder;
import android.widget.Toast;

import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc.DataState;
import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc.IHeartRateDataReceiver;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IDeviceStateChangeReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IPluginAccessResultReceiver;

public class HeartRateMonitor extends Service {
	
	public static final String BROADCAST_HEARTRATE = "hw.emote.hwutreasurehunt.heartratemonitor";
	public static final String HEARTRATE = "heartrate";
	
	// Heart rate monitor
	private AntPlusHeartRatePcc mHrPcc;
	private PccReleaseHandle<AntPlusHeartRatePcc> mReleaseHandle;
	private Activity mActivity;
	 /**
     * Intent to broadcast the location changes
     */
    private Intent mIntent;
	
    @Override
	public void onCreate() 
	{
		super.onCreate();
		mIntent = new Intent(BROADCAST_HEARTRATE); 
		//mActivity = activity;
		mHrPcc = null;
		mReleaseHandle = null;
		  //Release the old access if it exists
        if(mReleaseHandle != null)
        {
            mReleaseHandle.close();
        }

        requestAccessToPcc();
	}
		
	/*public HeartRateMonitor(Activity activity){
		// intent to broadcast the location changes
    	mIntent = new Intent(BROADCAST_HEARTRATE); 
		mActivity = activity;
		mHrPcc = null;
		mReleaseHandle = null;
	}
	    
	 /**
     * Resets the PCC connection to request access again and clears any existing display data.
     */
	 public void handleReset()
	 {
        //Release the old access if it exists
        if(mReleaseHandle != null)
        {
            mReleaseHandle.close();
        }

        requestAccessToPcc();
	 }
    
	 public void requestAccessToPcc()
	 {       
        // starts the plugins UI search
       // mReleaseHandle = AntPlusHeartRatePcc.requestAccess(getApplicationContext(),  
        //		base_IPluginAccessResultReceiver, base_IDeviceStateChangeReceiver);
	 }

	 /**
	  * Switches the active view to the data display and subscribes to all the data events
	  */
	 private void subscribeToHrEvents()
	 {
		 mHrPcc.subscribeHeartRateDataEvent(new IHeartRateDataReceiver()
		 {
            @Override
            public void onNewHeartRateData(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                final int computedHeartRate, final long heartBeatCount,
                final BigDecimal heartBeatEventTime, final DataState dataState)
            	{
                // Mark heart rate with asterisk if zero detected
                final String textHeartRate = String.valueOf(computedHeartRate)
                    + ((DataState.ZERO_DETECTED.equals(dataState)) ? "*" : "");

                // Mark heart beat count and heart beat event time with asterisk if initial value
                final String textHeartBeatCount = String.valueOf(heartBeatCount)
                    + ((DataState.INITIAL_VALUE.equals(dataState)) ? "*" : "");
                final String textHeartBeatEventTime = String.valueOf(heartBeatEventTime)
                    + ((DataState.INITIAL_VALUE.equals(dataState)) ? "*" : "");
                
                // update the intent
                mIntent.putExtra(HEARTRATE, textHeartRate);	
                sendBroadcast(mIntent);
                
                /*runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                    	Toast.makeText(getApplicationContext(), "Computed Heart Rate : " + textHeartRate,
                                 Toast.LENGTH_SHORT).show();
                    }
                });*/
            }            
		 });
    }
    
	// Releasing the handle to the heart rate monitor
	public void releaseHandle() {
		if(mReleaseHandle != null)
		{
		 mReleaseHandle.close();
	    }
	}
	 
    private IPluginAccessResultReceiver<AntPlusHeartRatePcc> base_IPluginAccessResultReceiver =
            new IPluginAccessResultReceiver<AntPlusHeartRatePcc>()
            {
            //Handle the result, connecting to events on success or reporting failure to user.
            @Override
            public void onResultReceived(AntPlusHeartRatePcc result, RequestAccessResult resultCode,
                DeviceState initialDeviceState)
            {
                switch(resultCode)
                {
                    case SUCCESS:
                        mHrPcc = result;
                        subscribeToHrEvents();
                        //Toast.makeText(getApplicationContext(), result.getDeviceName() + ": " + initialDeviceState, Toast.LENGTH_SHORT).show();
                        break;
                    case CHANNEL_NOT_AVAILABLE:
                        //Toast.makeText(getApplicationContext(), "Channel Not Available", Toast.LENGTH_SHORT).show();
                        break;
                    case ADAPTER_NOT_DETECTED:
                        //Toast.makeText(getApplicationContext(), "ANT Adapter Not Available. Built-in ANT hardware or external adapter required.", Toast.LENGTH_SHORT).show();
                        break;
                    case BAD_PARAMS:
                        //Note: Since we compose all the params ourself, we should never see this result
                        //Toast.makeText(getApplicationContext(), "Bad request parameters.", Toast.LENGTH_SHORT).show();
                        break;
                    case OTHER_FAILURE:
                        //Toast.makeText(getApplicationContext(), "RequestAccess failed. See logcat for details.", Toast.LENGTH_SHORT).show();
                        break;
                    case DEPENDENCY_NOT_INSTALLED:
                        /*AlertDialog.Builder adlgBldr = new AlertDialog.Builder(mActivity.getApplicationContext());
                        adlgBldr.setTitle("Missing Dependency");
                        adlgBldr.setMessage("The required service\n\"" + AntPluginPcc.getMissingDependencyName() + "\"\n was not found. You need to install the ANT+ Plugins service or you may need to update your existing version if you already have it. Do you want to launch the Play Store to get it?");
                        adlgBldr.setCancelable(true);
                        adlgBldr.setPositiveButton("Go to Store", new OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                Intent startStore = null;
                                startStore = new Intent(Intent.ACTION_VIEW,Uri.parse("market://details?id=" + AntPluginPcc.getMissingDependencyPackageName()));
                                startStore.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                mActivity.getApplicationContext().startActivity(startStore);
                            }
                        });
                        adlgBldr.setNegativeButton("Cancel", new OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                            }
                        });

                        final AlertDialog waitDialog = adlgBldr.create();
                        waitDialog.show();*/
                        break;
                    case USER_CANCELLED:
                    	 //Toast.makeText(getApplicationContext(), "Action cancelled", Toast.LENGTH_SHORT).show();
                        break;
                    case UNRECOGNIZED:
                        //Toast.makeText(getApplicationContext(), "Failed: UNRECOGNIZED. PluginLib Upgrade Required?", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        //Toast.makeText(getApplicationContext(), "Unrecognized result: " + resultCode, Toast.LENGTH_SHORT).show();
                        break;
                }
            }
            };

            //Receives state changes and shows it on the status display line
            protected  IDeviceStateChangeReceiver base_IDeviceStateChangeReceiver =
                new IDeviceStateChangeReceiver()
            {
                @Override
                public void onDeviceStateChange(final DeviceState newDeviceState)
                {
                    /*runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                        	Toast.makeText(getApplicationContext(),  
                        			"\nDevice Name : " + newDeviceState,
                                    Toast.LENGTH_SHORT).show();
                        }
                    });*/
                }
            };

			@Override
			public IBinder onBind(Intent intent) {
				// TODO Auto-generated method stub
				return null;
			}
}
