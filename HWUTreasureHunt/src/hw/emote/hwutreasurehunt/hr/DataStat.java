package hw.emote.hwutreasurehunt.hr;

import android.support.v4.util.ArrayMap;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * Created by Peter on 03/03/2016.
 */
public class DataStat implements Serializable {

    private static final long serialVersionUID = 1292149732089584692L;

    private String userId;

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public void setSteps(Long steps) {
        this.steps = steps;
    }

    private Long distance;
    private Long steps;
    private Map<Integer,PeriodicData> data;

    private Date start;
    private Date end;

    public DataStat() {
        data = new ArrayMap<Integer,PeriodicData>();
    }



    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }


    public String getUserId() {
        return userId;
    }

    public Long getDistance() {
        return distance;
    }

    public Long getSteps() {
        return steps;
    }

    public Map<Integer, PeriodicData> getData() {
        return data;
    }

    public void addData(int period, PeriodicData pData){
        data.put(period,pData);
    }
}
