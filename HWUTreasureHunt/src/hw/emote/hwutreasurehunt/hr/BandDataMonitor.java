//Subscribes to sensors, asks user consent for heart rate data and gets metrics such as heart rate, distance, skin temperature etc
package hw.emote.hwutreasurehunt.hr;

import android.content.Context;
import com.microsoft.band.sensors.*;

import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.util.Log;

import com.microsoft.band.BandClient;
import com.microsoft.band.BandClientManager;
import com.microsoft.band.BandException;
import com.microsoft.band.BandInfo;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.UserConsent;

import hw.emote.hwutreasurehunt.sensor.*;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.TextView;

public class BandDataMonitor extends SensorObservable {

    private BandClient client = null;
    private Button btnStart, btnConsent;
    private TextView txtStatus;
    private Context ctx;
    public BandDataMonitor(Context context) {
        this.ctx = context;
    }

    DataStatCollector collector = new DataStatCollector();
    PeriodicData periodicData = new PeriodicData();

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public class MyRunnable implements Runnable {
        private DataStatCollector dCollector;
        public MyRunnable(DataStatCollector _dCollector) {
            this.dCollector = _dCollector;
        }

        public void run() {
            dCollector.addPeriodicData(periodicData);
        }
    }

    final ScheduledFuture beeperHandle =
            scheduler.scheduleAtFixedRate(new MyRunnable(collector), 10, 10, TimeUnit.SECONDS);


    //Listens heart rate and prints to log via appendToUI method
    private BandHeartRateEventListener mHeartRateEventListener = new BandHeartRateEventListener() {
            @Override
            public void onBandHeartRateChanged(final BandHeartRateEvent event) {
               if (event != null) {

                    //Set our observer as changed and then notify them of captured value
                    setChanged();
                    notifyObservers(new SensorValue(
                        SensorValue.Type.HEART_RATE,
                        new Object[] {
                            new Float( event.getHeartRate() )
                        }
                    ));
                    appendToUI("Heart Rate =" +event.getHeartRate()+ " beats per minute\n"
                            + "Quality = " + event.getQuality());
                   periodicData.setHeartRate(event.getHeartRate());
                }
            }
        };

    //Listens distance and prints to log via appendToUI method
    private BandDistanceEventListener mDistanceEventListener = new BandDistanceEventListener() {
        @Override
        public void onBandDistanceChanged(final BandDistanceEvent event) {
            if (event != null) {
                appendToUI(String.format(" MotionType = %s, Speed  = %.3f, TotalDIstance = %d%n",
                        event.getMotionType(),event.getSpeed(), event.getTotalDistance()));
            }
        }
    };
    //Listens pedometer to get num of steps and prints to log via appendToUI method
    private BandPedometerEventListener mPedometerEventListener = new BandPedometerEventListener() {
        @Override
        public void onBandPedometerChanged(final BandPedometerEvent event) {
            if (event != null) {
                appendToUI("Steps:" + event.getTotalSteps());
            }
        }
    };
    //Listens skin temperature to get temperature
    private BandSkinTemperatureEventListener mSkinTemperatureEventListener = new BandSkinTemperatureEventListener() {
        @Override
        public void onBandSkinTemperatureChanged(final BandSkinTemperatureEvent event) {
            if (event != null) {
                appendToUI("Skin Temp: " + event.getTemperature());
                periodicData.setSkin(event.getTemperature());
            }
        }
    };
    //Listens skin temperature to get temperature
    private BandUVEventListener mUVEventListener = new BandUVEventListener() {
        @Override
        public void onBandUVChanged(final BandUVEvent event) {
            if (event != null) {
                appendToUI("UV: " + event.getUVIndexLevel());
                periodicData.setUv(event.getUVIndexLevel().toString());
            }
        }
    };


    private BandAmbientLightEventListener mAmbientLightEventListener = new BandAmbientLightEventListener() {
        @Override
        public void onBandAmbientLightChanged(final BandAmbientLightEvent event) {
            if (event != null) {
                appendToUI(String.format("Brightness = " + event.getBrightness() + "lux"));
                periodicData.setAmbientLight(event.getBrightness());
            }
        }
    };


    public  void initialize(Activity activity) {
        Log.d("TreasureHunt", "Initialize band");

        final WeakReference<Activity> reference = new WeakReference<Activity>(activity);
                new HeartRateConsentTask().execute(reference);
    }


    public void finish(){
        Log.d("TreasureHunt", "Finish band");


        new DataUnSubscriptionTask().execute();
    }




    private class DataSubscriptionTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (getConnectedBandClient()) {
                    if (client.getSensorManager().getCurrentHeartRateConsent() == UserConsent.GRANTED) {
                        collector.start();
                        client.getSensorManager().registerHeartRateEventListener(mHeartRateEventListener);
                        client.getSensorManager().registerPedometerEventListener(mPedometerEventListener);
                        client.getSensorManager().registerDistanceEventListener(mDistanceEventListener);
                        client.getSensorManager().registerSkinTemperatureEventListener(mSkinTemperatureEventListener);
                        client.getSensorManager().registerUVEventListener(mUVEventListener);
                        client.getSensorManager().registerAmbientLightEventListener(mAmbientLightEventListener);
                    } else {
                        Log.d("TreasureHunt","You have not given this application consent to access heart rate data yet."
                                + " Please press the Heart Rate Consent button.\n");
                        appendToUI( "You have not given this application consent to access heart rate data yet." );
                    }
                } else {
                   Log.d("TreasureHunt","Band isn't connected. Please make sure bluetooth is on and the band is in range.\n");
                   appendToUI( "Band isn't connected. Please make sure bluetooth is on and the band is in range.\n" );
                }
            } catch (BandException e) {
                String exceptionMessage="";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                        break;
                }
                appendToUI(exceptionMessage);

            } catch (Exception e) {
                appendToUI(e.getMessage());
            }
            return null;
        }
    }
    private class DataUnSubscriptionTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (getConnectedBandClient()) {

                        client.getSensorManager().unregisterHeartRateEventListener(mHeartRateEventListener);
                        client.getSensorManager().unregisterPedometerEventListener(mPedometerEventListener);
                        client.getSensorManager().unregisterDistanceEventListener(mDistanceEventListener);
                        client.getSensorManager().unregisterSkinTemperatureEventListener(mSkinTemperatureEventListener);
                        client.getSensorManager().unregisterUVEventListener(mUVEventListener);
                        client.getSensorManager().unregisterAmbientLightEventListener(mAmbientLightEventListener);
                        collector.finish();

                } else {
                    Log.d("TreasureHunt","Band isn't connected. Please make sure bluetooth is on and the band is in range.\n");
                    appendToUI( "Band isn't connected. Please make sure bluetooth is on and the band is in range.\n" );
                }
            } catch (BandException e) {
                String exceptionMessage="";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                        break;
                }
                appendToUI(exceptionMessage);

            } catch (Exception e) {
                appendToUI(e.getMessage());
            }
            return null;
        }
    }

    private class HeartRateConsentTask extends AsyncTask<WeakReference<Activity>, Void, Void> {
        @Override
        protected Void doInBackground(WeakReference<Activity>... params) {
            try {
                if (getConnectedBandClient()) {

                    if (params[0].get() != null) {
                        client.getSensorManager().requestHeartRateConsent(params[0].get(), new HeartRateConsentListener() {
                            @Override
                            public void userAccepted(boolean consentGiven) {
                                Log.d("TreasureHunt","User accepted: " + consentGiven);
                                if(consentGiven){
                                    new DataSubscriptionTask().execute();
                                }
                            }
                        });
                    }
                } else {
                   Log.d("TreasureHunt","Band isn't connected. Please make sure bluetooth is on and the band is in range.\n");
                }
            } catch (BandException e) {
                String exceptionMessage="";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                        break;
                }
                Log.d("TreasureHunt",exceptionMessage);
                //appendToUI(exceptionMessage);

            } catch (Exception e) {
                Log.d("TreasureHunt",e.getMessage());
               // appendToUI(e.getMessage());
            }
            return null;
        }
    }

    private void appendToUI(final String string) {
        Log.d("TreasureHunt",string);
//        setChanged();
//        notifyObservers(new SensorStatus(
//            SensorStatus.Status.MESSAGE,
//            string
//        ));
    }

    private boolean getConnectedBandClient() throws InterruptedException, BandException {
        if (client == null) {
            BandInfo[] devices = BandClientManager.getInstance().getPairedBands();
            if (devices.length == 0) {
                appendToUI("Band isn't paired with your phone.\n");
                return false;
            }
            client = BandClientManager.getInstance().create(ctx, devices[0]);
        } else if (ConnectionState.CONNECTED == client.getConnectionState()) {
            return true;
        }

        appendToUI("Band is connecting...\n");
        return ConnectionState.CONNECTED == client.connect().await();
    }

    /**
     * Called to reset the band.
     * Was used in ANT+ might not be relevant to MS band.
    */
    public void handleReset( Activity activity ) {
    }

    /**
     * Called to stop the band service.
     * Was used in ANT+ but might be relvant to MS band.
    */
    public void stop() {
    }
}

