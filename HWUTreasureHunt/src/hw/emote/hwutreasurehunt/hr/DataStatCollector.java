package hw.emote.hwutreasurehunt.hr;

import android.util.Log;

import java.util.Date;

/**
 * Created by Peter on 03/03/2016.
 */
public class DataStatCollector {


    private int period = 0;
    private DataStat stat;


    private boolean firstStep;
    private boolean firstDistance;
    private int steps;
    private int distance;


    public DataStatCollector(){
        stat = new DataStat();
        firstStep = false;
        firstDistance = false;
    }


    public void start(){
        stat.setStart(new Date());
    }

    public void finish(){
        StatDao statdao= new StatDao(); //connect to MongoDB
        stat.setEnd(new Date());
        Log.d("TreasureHunt",stat.toString());
        statdao.saveMetrics(stat); //write DataStat object to mongodb
    }


    public void addPeriodicData(PeriodicData a){
        stat.addData(period,a);
        period+=1;
    }

    public void setStep(int step){
        if(!firstStep){
            steps= step;
            firstStep = true;
        }
    }

    public void setDistance(int d){
        if(!firstDistance){
            distance= d;
            firstDistance = true;
        }
    }








}
