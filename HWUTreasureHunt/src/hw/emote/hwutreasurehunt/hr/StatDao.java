package hw.emote.hwutreasurehunt.hr;



import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class StatDao {

    private static final String url = "https://protected-lake-87247.herokuapp.com/metrics";

    public StatDao() {

    }


    public void saveMetrics(DataStat stat) {
        InputStream inputStream = null;
        try {

            URL object=new URL(url);

            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");


           // JSONObject parent=new JSONObject();
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();

           // parent.put("distance","12345");

//            OutputStreamWriter wr= new OutputStreamWriter(con.getOutputStream());
//            wr.write(gson.toJson(stat));


            OutputStream os = con.getOutputStream();
            Log.d("TreasureHunt",gson.toJson(stat).toString());
            os.write(gson.toJson(stat).toString().getBytes("UTF-8"));
            os.close();

            int httpResult = con.getResponseCode();
            Log.d("TreasureHunt",Integer.toString(httpResult));



        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        Log.d("TreasureHunt", "saveMetrics\n");
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }


}
