package hw.emote.hwutreasurehunt.hr;

import hw.emote.dialogsystem.AndroidDialogInterface;
import hw.emote.dialogsystem.AndroidDialogProvider;
import hw.emote.dialogsystem.DialogSystem;
import hw.emote.eatreasurehunt.R;
import hw.emote.feedbackparser.FeedbackItem;
import hw.emote.feedbackparser.FeedbackTemplate;
import hw.emote.feedbackparser.XMLFeedbacksReader;
import hw.emote.hwutreasurehunt.HWUGeneral;
import hw.emote.hwutreasurehunt.LogManager;
import hw.emote.hwutreasurehunt.location.LocationService;
import hw.emote.routeparser.Point;
import hw.emote.routeparser.Question;
import hw.emote.routeparser.Route;
import hw.emote.routeparser.Step;
import hw.emote.routeparser.XMLRouteReader;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;

import uk.ac.hw.lirec.emys3d.EmysModel.Emotion;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.hardware.GeomagneticField;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc.DataState;
import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc.IHeartRateDataReceiver;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IDeviceStateChangeReceiver;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc.IPluginAccessResultReceiver;

/**
 * A class to record heart rates while user goes around the route
 * @author Meiyii
 *
 */

public class HeartRateActivity extends Activity implements 
	OnTouchListener {
	
	private static final String TAG = "HWUTreasureHuntActivity";
	private final List<String> mPoints = new ArrayList<String>(
			Arrays.asList("Gait 4", "Old Mile Post", "Gait 3", "Sunken Garden", "Sundial Base", "Library",
					"Cameron Smail Road", "Freeway", "Lord Balerno Building", "James Watt", "Clever Cow", "Gait 9", "Stone of Whales", 
					"Loch", "Bridge", "May Cippico Fountain", "Gait 2", "Stone of Ravenscraig", "Earl Mountbatten"));
	
	// Intent
	private Intent mIntent;
	
	// Layout components
	private ImageView mTreasureMap;
	private ScrollView mScroll;
	private TextView mTxtSubtitles;
	private TextView mTxtNext;
	
	// Translate and zoom related variables
	private static final float MIN_ZOOM = 0.5f;
	private static final float MAX_ZOOM = 2.0f;
	private Matrix mMatrix = new Matrix();
	private Matrix mSavedMatrix = new Matrix();
	private Matrix mScaleMatrix = new Matrix();
	private Matrix mSavedScaleMatrix = new Matrix();
	private PointF mStartPoint = new PointF();
	private PointF mMidPoint = new PointF();
	private float mOldDist = 1f;
	private float mScale = 1f;
	private static final int NONE = 0;
	private static final int DRAG = 1;
	private static final int ZOOM = 2;
	private int mode = NONE;
	private boolean mLoadMap;
	
	// External Storage Log 
	private LogManager mEALogManager;
	private String mLogFileName;
 	
 	// user position
    private Location mCurrentLocation;
    private Location mPreviousLocation;
	 	
	// Heart rate monitor
	private AntPlusHeartRatePcc mHrPcc = null;
    private PccReleaseHandle<AntPlusHeartRatePcc> mReleaseHandle = null;
    private float mHeartRate;
	private String mUserID;
	private String mSelectedCondition;
	private String mSelectedRoute;
	private String mPoint;
	private int mIndex = 0;
	private int mPointIndex = 1;
	private Timer mTimer;
	private TimerTask mTask;
    private Button mBtnGPSCoordinates;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Retrieve information passed over from the setup activity
		mIntent = getIntent();
		mUserID = mIntent.getStringExtra(HWUGeneral.USER_ID);
		mSelectedCondition = mIntent.getStringExtra(HWUGeneral.CONDITION);
		mSelectedRoute = mIntent.getStringExtra(HWUGeneral.ROUTE);
		mLogFileName = mIntent.getStringExtra(HWUGeneral.LOG_FILE);
		//Toast.makeText(getApplicationContext(), "machine IP :: "+ mMachineIP, Toast.LENGTH_LONG).show();
		//Log.d(TAG,"user id " + mUserID + " feedback " + mSelectedFeedbackType + " route " + mSelectedRoute );
		
		// Initialise the layout components
		setContentView(R.layout.activity_heart_rate);
		
		mTreasureMap = (ImageView) findViewById(R.id.treasureMap); 
		//mMapScale = (ImageView) findViewById(R.id.mapScale);
    	mTreasureMap.setOnTouchListener(this);       
    	mTreasureMap.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
    	    @Override
    	    public void onGlobalLayout() {
    	       // center the map upon load
    	       if (!mLoadMap) {
	    	       int width = mTreasureMap.getWidth();  
	    	       //Toast.makeText(getApplicationContext(), "map width :: "+ width + " scale width :: " + mMapScale.getWidth(), Toast.LENGTH_LONG).show();
	    	       mMatrix.set(mSavedMatrix);
	    	       mScaleMatrix.set(mSavedScaleMatrix);
				   mMatrix.postTranslate(mStartPoint.x - width/3, mStartPoint.y);
				   mTreasureMap.setImageMatrix(mMatrix);
				   //mMapScale.setImageMatrix(mScaleMatrix);
				   mLoadMap = true; // map has been loaded
    	       }
    	    }
    	});
    	   	
    	mScroll = (ScrollView) findViewById(R.id.hrScroll);    
    	mScroll.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
	           @Override
	           public void onGlobalLayout() {
	               // Ready, move up
	               mScroll.fullScroll(View.FOCUS_UP);
	           }
	       });
    	mTxtSubtitles = (TextView) findViewById(R.id.txtSubtitles);
    	mTxtNext = (TextView) findViewById(R.id.txtNext);
    	
    	// user locations
    	mCurrentLocation = new Location("Current");
    	mCurrentLocation.setLatitude(0.0);
    	mCurrentLocation.setLongitude(0.0);
    	mPreviousLocation = new Location("Previous");
    	mPreviousLocation.setLatitude(0.0);
    	mPreviousLocation.setLongitude(0.0);
        //registerReceiver(testlocationReceiver, new IntentFilter(TEST_BROADCAST_LOCATION));
    	
        // start the location service - only once as it will be running continuously in the 
        // background until the app is destroyed
        startLocationService();        
        
        handleReset();
        mHeartRate = 0.0f;  
        
	    //********** GPS button Click Event - Record GPS coordinates **********
    	// intent to broadcast the location changes
        mBtnGPSCoordinates = (Button) findViewById(R.id.btnGPSCoordinates);
        mBtnGPSCoordinates.setOnClickListener(new View.OnClickListener() {
 
            @Override
			public void onClick(View view) {
            	mBtnGPSCoordinates.setText("Next");
            	mTxtSubtitles.setText("Heart Rate: " + mHeartRate);
            	if (mIndex < mPoints.size()) {
            		mPoint = mPoints.get(mIndex++);
            		mTxtNext.setText("Next Point: \n" + mPointIndex++ + ". " + mPoint);
            	} else {
            		Toast.makeText(getApplicationContext(), "You have reached the end!", Toast.LENGTH_LONG).show();
            	}
            	if (mPoint != null) {
            		mEALogManager.writeAtPointToHRFile(mCurrentLocation, mPoint, mHeartRate);
            	}
            }
        });
	}    
    
	 /**
     * Resets the PCC connection to request access again and clears any existing display data.
     */
    private void handleReset()
    {
        //Release the old access if it exists
        if(mReleaseHandle != null)
        {
            mReleaseHandle.close();
        }

        requestAccessToPcc();
    }
    
    private void requestAccessToPcc()
    {
        Intent intent = getIntent();
       
        // starts the plugins UI search
        mReleaseHandle = AntPlusHeartRatePcc.requestAccess(this, this,
        		base_IPluginAccessResultReceiver, base_IDeviceStateChangeReceiver);
    }

    /**
     * Switches the active view to the data display and subscribes to all the data events
     */
    private void subscribeToHrEvents()
    {
        mHrPcc.subscribeHeartRateDataEvent(new IHeartRateDataReceiver()
        {
            @Override
            public void onNewHeartRateData(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                final int computedHeartRate, final long heartBeatCount,
                final BigDecimal heartBeatEventTime, final DataState dataState)
            {
                // Mark heart rate with asterisk if zero detected
            	mHeartRate = computedHeartRate;
                //mHeartRate = String.valueOf(computedHeartRate)
                //    + ((DataState.ZERO_DETECTED.equals(dataState)) ? "*" : "");

                // Mark heart beat count and heart beat event time with asterisk if initial value
                final String textHeartBeatCount = String.valueOf(heartBeatCount)
                    + ((DataState.INITIAL_VALUE.equals(dataState)) ? "*" : "");
                final String textHeartBeatEventTime = String.valueOf(heartBeatEventTime)
                    + ((DataState.INITIAL_VALUE.equals(dataState)) ? "*" : "");
                
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                    	mTxtSubtitles.setText("Heart Rate: " + mHeartRate);
                    }
                });           
            }
        });
    }
    
    private IPluginAccessResultReceiver<AntPlusHeartRatePcc> base_IPluginAccessResultReceiver =
            new IPluginAccessResultReceiver<AntPlusHeartRatePcc>()
            {
            //Handle the result, connecting to events on success or reporting failure to user.
            @Override
            public void onResultReceived(AntPlusHeartRatePcc result, RequestAccessResult resultCode,
                DeviceState initialDeviceState)
            {
                switch(resultCode)
                {
                    case SUCCESS:
                        mHrPcc = result;
                        subscribeToHrEvents();
                        Toast.makeText(getApplicationContext(), result.getDeviceName() + ": " + initialDeviceState, Toast.LENGTH_SHORT).show();
                        break;
                    case CHANNEL_NOT_AVAILABLE:
                        Toast.makeText(getApplicationContext(), "Channel Not Available", Toast.LENGTH_SHORT).show();
                        break;
                    case ADAPTER_NOT_DETECTED:
                        Toast.makeText(getApplicationContext(), "ANT Adapter Not Available. Built-in ANT hardware or external adapter required.", Toast.LENGTH_SHORT).show();
                        break;
                    case BAD_PARAMS:
                        //Note: Since we compose all the params ourself, we should never see this result
                        Toast.makeText(getApplicationContext(), "Bad request parameters.", Toast.LENGTH_SHORT).show();
                        break;
                    case OTHER_FAILURE:
                        Toast.makeText(getApplicationContext(), "RequestAccess failed. See logcat for details.", Toast.LENGTH_SHORT).show();
                        break;
                    case DEPENDENCY_NOT_INSTALLED:
                        AlertDialog.Builder adlgBldr = new AlertDialog.Builder(getApplicationContext());
                        adlgBldr.setTitle("Missing Dependency");
                        adlgBldr.setMessage("The required service\n\"" + AntPluginPcc.getMissingDependencyName() + "\"\n was not found. You need to install the ANT+ Plugins service or you may need to update your existing version if you already have it. Do you want to launch the Play Store to get it?");
                        adlgBldr.setCancelable(true);
                        adlgBldr.setPositiveButton("Go to Store", new OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                Intent startStore = null;
                                startStore = new Intent(Intent.ACTION_VIEW,Uri.parse("market://details?id=" + AntPluginPcc.getMissingDependencyPackageName()));
                                startStore.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                getApplicationContext().startActivity(startStore);
                            }
                        });
                        adlgBldr.setNegativeButton("Cancel", new OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                            }
                        });

                        final AlertDialog waitDialog = adlgBldr.create();
                        waitDialog.show();
                        break;
                    case USER_CANCELLED:
                    	 Toast.makeText(getApplicationContext(), "Action cancelled",
                                 Toast.LENGTH_SHORT).show();
                        break;
                    case UNRECOGNIZED:
                        Toast.makeText(getApplicationContext(), "Failed: UNRECOGNIZED. PluginLib Upgrade Required?",
                            Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(getApplicationContext(), "Unrecognized result: " + resultCode, Toast.LENGTH_SHORT).show();
                        break;
                }
            }
            };

            //Receives state changes and shows it on the status display line
            protected  IDeviceStateChangeReceiver base_IDeviceStateChangeReceiver =
                new IDeviceStateChangeReceiver()
            {
                @Override
                public void onDeviceStateChange(final DeviceState newDeviceState)
                {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                        	Toast.makeText(getApplicationContext(),  
                        			"\nDevice Name : " + newDeviceState,
                                    Toast.LENGTH_SHORT).show();
                        }
                    });


                }
            };
        
    @Override
	public void onResume() {      
	    super.onResume();
	    
	    // Log Manager
	 	mEALogManager = new LogManager();
	 	if (mEALogManager.checkStorageStatus()) {
	 		mEALogManager.createHRFile(mLogFileName + ".txt");
	 	}
	 	mTask = new TimerTask() {
            @Override
            public void run() {                    	
               	if (mCurrentLocation != null && mPoint != null && mEALogManager != null) {
            		mEALogManager.writeToHRFile(mCurrentLocation, mPoint, mHeartRate);
            	}
            }
        };

        mTimer = new Timer();
        mTimer.schedule(mTask, 5000, 1000); 
	}
	
	@Override
	public void onPause() {
	    super.onPause();
	    
	    if (mTimer != null) {
	    	mTimer.cancel();
	    }
	    // Close the log file
	    mEALogManager.closeHRFile();
	}
	
	@Override
	public void onDestroy() {
		stopAllServices();
		super.onDestroy();
		Log.d(TAG,"App destroyed");
	}
	

    
	/**
	 * Stop all services
	 */
	private void stopAllServices() {
		// release the access to heart rate monitor
		if(mReleaseHandle != null)
        {
             mReleaseHandle.close();
        }	   
	    stopLocationService();
	}
	
	/**
	 * Prevent the user from closing the app during the hunt
	 */
	/*@Override
	public void onBackPressed() {
	   return;
	}*/
        	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.eatreasure_hunt, menu);
		return true;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageView view = (ImageView) v;
    	switch (event.getAction() & MotionEvent.ACTION_MASK) {
    		// user touches the screen
    		case MotionEvent.ACTION_DOWN:

    			mSavedMatrix.set(mMatrix);
    			mSavedScaleMatrix.set(mScaleMatrix);
    			//Log.d(TAG,"mSavedMatrix=" + mSavedMatrix.toString());
    			mStartPoint.set(event.getX(), event.getY());
    			mode = DRAG;
    			break;
    			
    		// user touches the screen with two fingers 
    		case MotionEvent.ACTION_POINTER_DOWN:
	
    			mOldDist = spacing(event);
	
    			if (mOldDist > 10f) {
    				mSavedMatrix.set(mMatrix);
    				mSavedScaleMatrix.set(mScaleMatrix);
    				midPoint(mMidPoint, event);
    				mode = ZOOM;
    			}
    			break;
	
    		// user releases the touch
    		case MotionEvent.ACTION_UP:
    			mode = NONE;
    			break;
	
    		case MotionEvent.ACTION_POINTER_UP:
    			mode = NONE;	
    			break;
	
    		case MotionEvent.ACTION_MOVE:    			
    			if (mode == DRAG) {       				    				
    				mMatrix.set(mSavedMatrix);
    				// calculate the distance for translation
    				float xDist = event.getX() - mStartPoint.x;
    				float yDist = event.getY() - mStartPoint.y;
    				
    				// retrieving the matrix scale and translations to check that the final values are within boundaries
    				float[] f = new float[9];    				
    				mMatrix.getValues(f);
    				float scale = f[Matrix.MSCALE_X];
    				float transX = f[Matrix.MTRANS_X];
    				float transY = f[Matrix.MTRANS_Y]; 
    				
    				// limit the translation boundaries taking into consideration the scale of the map
    				float mMaxX = mTreasureMap.getWidth()/2;
    				float mMinX = - (mTreasureMap.getWidth() * 1.5f) * scale;
    				float mMaxY = mTreasureMap.getHeight()/2;
    				float mMinY = - (mTreasureMap.getHeight() * 1.75f) * scale ;
    				//Log.d(TAG,"Max Min " + mMaxX + " " + mMinX + " " +  mMaxY + " " + mMinY + " mScale " + scale);    
    				//Log.d(TAG,"xDist yDist before" + xDist + " " + yDist);
    				
    				// calculate the new position
    				float totalX = transX + xDist;
    				float totalY = transY + yDist;
					
    				// setting the boundaries
    				if (totalX > mMaxX) {
						xDist = mMaxX-totalX;    					
					}
					if (totalX < mMinX) {
						xDist = mMinX-totalX;
					}
					if (totalY > mMaxY) {
						yDist = mMaxY-totalY;
					}
					if (totalY < mMinY) {
						yDist = mMinY-totalY;
					}
					
					// apply the translation
					mMatrix.postTranslate(xDist, yDist); 
					//Log.d(TAG,"xDist yDist after" + xDist + " " + yDist + " " + transX + " " + transY);
    				//Log.d(TAG,"matrix=" + mMatrix.toString());					
    			} else if (mode == ZOOM) {
    				float[] f = new float[9];
    				float newDist = spacing(event);
    				mScale = 1f;
    			
    				if (newDist > 10f) {
    					mMatrix.set(mSavedMatrix);
    					mScaleMatrix.set(mSavedScaleMatrix);
    					mScale = newDist / mOldDist;
    					//Log.d(TAG,"newDist " + newDist + " oldDist " + mOldDist);
    					mMatrix.postScale(mScale, mScale, mMidPoint.x, mMidPoint.y);
    					mScaleMatrix.postScale(mScale, 1.0f, 0.0f, 0.0f);    					
    				}
    				
    				// retrieving the matrix scales
    				mMatrix.getValues(f);
    				float scaleX = f[Matrix.MSCALE_X];
    				//float scaleY = f[Matrix.MSCALE_Y];
    				
    				// the minimum and maximum zoom levels
    				if(scaleX <= MIN_ZOOM) {
    					mScale = MIN_ZOOM/scaleX;
    				}    	
    				else if(scaleX >= MAX_ZOOM) {
    					mScale = MAX_ZOOM/scaleX;
       				} 
    				mMatrix.postScale(mScale, mScale, mMidPoint.x, mMidPoint.y);
    				mScaleMatrix.postScale(mScale, 1.0f, 0.0f, 0.0f); 
    			}    			
    			break;	
	     }
    	 view.setImageMatrix(mMatrix);
    	 //mMapScale.setImageMatrix(mScaleMatrix);
	     return true;
	}
	
	/**
	 * Determine the space between the first two fingers
	 */
	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
	    float y = event.getY(0) - event.getY(1);
	    return (float)Math.sqrt(x * x + y * y);
	}

	/**
	 * Calculate the mid point of the first two fingers
	 */
	private void midPoint(PointF point, MotionEvent event) {
	    float x = event.getX(0) + event.getX(1);
	   	float y = event.getY(0) + event.getY(1);
	   	point.set(x / 2, y / 2);
	}
    
	/**
	 * Start the location service
	 */
	private void startLocationService() {
		// start the location service - only once as it will be running continuously in the 
        // background until the app is destroyed
	    Intent locationService = new Intent( this, LocationService.class );
	    locationService.putExtra(HWUGeneral.LOG_FILE, mLogFileName);
	    startService(locationService);
	    // register broadcast receiver for location
	    registerReceiver(locationReceiver, new IntentFilter(LocationService.BROADCAST_LOCATION));
	}

	/**
	 * Stop the location service
	 */
	private void stopLocationService(){
		// stop the location service
	    Intent locationService = new Intent( this, LocationService.class );
	    stopService(locationService);
	    // unregister broadcast receiver
	    unregisterReceiver(locationReceiver);
	}

    /** 
	 * Broadcast receiver for user location 
	 */
    private BroadcastReceiver locationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {   
        	// copy the current location
        	mPreviousLocation.set(mCurrentLocation);
        	Log.d(TAG,"previous latitude: " + mPreviousLocation.getLatitude() + "longitude: " + mPreviousLocation.getLongitude());
        	Log.d(TAG,"current latitude: " + intent.getDoubleExtra("latitude", 0.0) + "longitude: " + intent.getDoubleExtra("longitude", 0.0));
        	mCurrentLocation.setLatitude(intent.getDoubleExtra(LocationService.LATITUDE, 0.0));
        	mCurrentLocation.setLongitude(intent.getDoubleExtra(LocationService.LONGITUDE, 0.0));
        }
    };
}
