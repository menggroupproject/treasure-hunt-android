package hw.emote.hwutreasurehunt;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import hw.emote.routeparser.Route;

public class DownloadImage extends AsyncTask<String, Integer, AsyncTaskResult<Drawable>> {
    public DownloadImageDelegate delegate;

    public interface DownloadImageDelegate {
        public void imageDownloaded(Drawable image);
        public void imageNotDownloaded();
    }

    @Override
    protected AsyncTaskResult<Drawable> doInBackground(String... arg0) {
        try {
            BitmapDrawable image = (BitmapDrawable) downloadImage(arg0[0]);
            if (image.getBitmap().getWidth() <= 0) {
                throw new Exception("image not found");
            } else {
                return new AsyncTaskResult<Drawable>(image);
            }
        } catch (Exception anyError) {
            return new AsyncTaskResult<Drawable>(anyError);
        }
    }

    /**
     * Called after the image has been downloaded
     * -> this calls a function on the main thread again
     */
    protected void onPostExecute(AsyncTaskResult<Drawable> result) {
        if (result.getError() != null || result.getResult() == null) {
            delegate.imageNotDownloaded();
        } else {
            delegate.imageDownloaded(result.getResult());
        }
    }


    /**
     * Actually download the Image from the _url
     * @param _url
     * @return
     */
    private Drawable downloadImage(String _url) throws Exception
    {
        //Prepare to download image
        URL url;
        BufferedOutputStream out;
        InputStream in;
        BufferedInputStream buf;

        //BufferedInputStream buf;
        url = new URL("http://protected-lake-87247.herokuapp.com/image?id=" + _url);
        URLConnection urlConnection = url.openConnection();
        urlConnection.setConnectTimeout(10000);
        urlConnection.setReadTimeout(10000);

        in = urlConnection.getInputStream();
        buf = new BufferedInputStream(in);

        // Convert the BufferedInputStream to a Bitmap
        Bitmap bMap = BitmapFactory.decodeStream(buf);
        if (in != null) {
            in.close();
        }
        if (buf != null) {
            buf.close();
        }

        return new BitmapDrawable(bMap);
    }

}

