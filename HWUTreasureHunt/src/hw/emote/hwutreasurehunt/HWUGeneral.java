package hw.emote.hwutreasurehunt;

/**
 * @author Mei Yii Lim
 *
 * This is class contains common variables used in the hwutreasurehunt App
 */

public class HWUGeneral {
	// Data variables
	public final static String CONDITION = "hw.emote.hwutreasurehunt.CONDITION";
	public final static String ROUTE = "hw.emote.hwutreasurehunt.ROUTE";
	public final static String USER_ID = "hw.emote.hwutreasurehunt.USER_ID";
	public final static String LOG_FILE = "hw.emote.hwutreasurehunt.LOG_FILE";
	public final static String TREASURE_HUNT_MODE = "hw.emote.hwutreasurehunt.TREASURE_HUNT_MODE";
	public final static String CURRENT_STEP = "hw.emote.hwutreasurehunt.CURRENT_STEP";
	public final static String CURRENT_QUESTION = "hw.emote.hwutreasurehunt.CURRENT_QUESTION";
	public final static String CURRENT_POINT = "hw.emote.hwutreasurehunt.CURRENT_POINT";
	public final static String CORRECT_ANSWERS = "hw.emote.hwutreasurehunt.CORRECT_ANSWERS";
	public final static String TOTAL_CORRECT = "hw.emote.hwutreasurehunt.TOTAL_CORRECT";
	public final static String TOTAL_QUESTIONS = "hw.emote.hwutreasurehunt.TOTAL_QUESTIONS";
	public final static String TOTAL_UNLOCKED = "hw.emote.hwutreasurehunt.TOTAL_UNLOCKED";
	public final static String TOTAL_TREASURES = "hw.emote.hwutreasurehunt.TOTAL_TREASURES";
	public final static String INTERVENTIONS = "hw.emote.hwutreasurehunt.INTERVENTIONS";
	public final static String TOTAL_TREASURE_CLICKS = "hw.emote.hwutreasurehunt.TOTAL_TREASURE_CLICKS";
	public final static String TOTAL_CLICK_TREASURES = "hw.emote.hwutreasurehunt.TOTAL_CLICK_TREASURES";
	public final static String NI_POINTS = "hw.emote.hwutreasurehunt.NI_POINTS";
	public final static String TOTAL_ADDED = "hw.emote.hwutreasurehunt.TOTAL_ADDED";
	public final static String PROCEED_STRING = "hw.emote.hwutreasurehunt.PROCEED_STRING";
	public final static String PRIZE_Q_ANSWERED = "hw.emote.hwutreasurehunt.PRIZE_Q_ANSWERED";
	public final static String AT_DESTINATION = "hw.emote.hwutreasurehunt.AT_DESTINATION";
	public final static String MIGRATED = "hw.emote.hwutreasurehunt.MIGRATED";
	public final static String BASELINE_HR = "hw.emote.hwutreasurehunt.BASELINE_HR";
	public final static String RUNNING_MODE = "hw.emote.hwutreasurehunt.RUNNING_MODE";
	
	// Treasure hunt modes
	public static final int CLUE = 0;
	public static final int QUESTION = 1;
	public static final int ANSWER = 2;
	public static final int CHANGE = 3;
	public static final int BASELINE = 4;
	public static final int SOLUTION = 5;

	// Treasure hunt running modes
	public enum TreasureHuntRunningMode {
		DEMONSTRATION,
		REAL,
	}
	
	// Setup variables
	public final static String AFFECTIVE = "affective";
	public final static String EMPATHIC = "empathic";
	public final static String NON_EMPATHIC = "nonEmpathic";
	public final static String ROUTE_1 = "Route 1";
	public final static String ROUTE_2 = "Route 2";

	// Feedback and Route files
	public final static String FEEDBACKS_FILE = "feedbacks.xml"; 
	public final static String ROUTES_FILE = "routes.xml";
	
	// Shared Preference files
	public static final String SETUP_FILE = "MySetupFile";
	public static final String DATA_FILE = "MyDataFile";	
	
	// Q & A variables
	public static final int MAX_CLUE = 8;
	public static final int EMBLEMS_NO = 3;
	public static final int INTERVENTION_NO = 5;  
	public static final int TREASURE_THRESHOLD = 6;
	public static final int HR_RECORD_TIME = 60;
	
	// Reasons for interventions
	public final static String WRONG_DIRECTION = "wrong_dir";
	public final static String CONFUSED = "confused";
	public final static String NOT_AT_DESTINATION = "not_at_dest";
	public final static String CLUE_NOT_FOUND = "clue_not_found";
	public final static String PASSED_DESTINATION = "passed_dest";
	
	// User movement
	public final static String MOVING = "moving";
	public final static String STATIONARY = "stationary";
}
