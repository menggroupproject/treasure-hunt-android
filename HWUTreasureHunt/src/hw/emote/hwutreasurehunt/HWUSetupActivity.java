package hw.emote.hwutreasurehunt;

import hw.emote.eatreasurehunt.R;
import hw.emote.routeparser.Route;
import hw.emote.routeparser.XMLRouteReader;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * @author Mei Yii Lim
 *
 * This is the setup activity for the EATreasureHunt application.
 *
 * It allows configuration of the experimental conditions: Memory, Route and userID
 * When user presses the "Go to Treasure Hunt button", it will start the EATreasureHunt Activity.
 */

public class HWUSetupActivity extends Activity implements RouteService.RouteDownloadDelegate {

    // Interface components
    private RadioGroup mRadioCondition;
    private Spinner mRouteSpinner;
	private Button mRouteDownloadButton;
    private EditText mEditUserID;
    private Button mBtnStartApp;
    private Button mBtnStartAppDemonstration;
    private Button mBtnReconfigureApp;
    private TextView mSetupError;

    // Setup data
    private int mCondition;
    private Route selectedRoute;
    private String mUserID;
    private String mMachineIP;
    private String mLogFileName;
    private RouteService routeService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_easetup);

        mRadioCondition = (RadioGroup) findViewById(R.id.radioCondition);
        mRouteSpinner = (Spinner) findViewById(R.id.routeSpinner);
		mRouteDownloadButton = (Button) findViewById(R.id.downloadRoutesButton);
        mEditUserID = (EditText) findViewById(R.id.editUserID);
        mBtnStartApp = (Button) findViewById(R.id.btnStartApp);
        mBtnStartAppDemonstration = (Button) findViewById(R.id.btnStartAppDemonstration);
        mBtnReconfigureApp = (Button) findViewById(R.id.btnReconfigureApp);
        mSetupError = (TextView) findViewById(R.id.txtSetupError);

        // Start button Click Event - Go to the Treasure Hunt Activity
        mBtnStartApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                treasureHuntButtonPressed(HWUGeneral.TreasureHuntRunningMode.REAL);
            }
        });

        mBtnStartAppDemonstration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                treasureHuntButtonPressed(HWUGeneral.TreasureHuntRunningMode.DEMONSTRATION);
            }
        });

        mBtnReconfigureApp.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                resetComponents();
                return false;
            }
        });

		mRouteDownloadButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				disableComponents();
				routeService.resetRoutes();
				routeService.fetchRoutes();
			}
		});

        routeService = RouteService.getInstance();
		routeService.setupService(this, getBaseContext());
        routeService.fetchRoutes();
    }

    @Override
    public void routesFetched(ArrayList<Route> routes) {
		resetComponents();
		ArrayAdapter<Route> routeArrayAdapter = new ArrayAdapter<Route>(this, android.R.layout.simple_spinner_item, routes);
        routeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mRouteSpinner.setAdapter(routeArrayAdapter);
        mRouteSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				selectedRoute = (Route) parent.getItemAtPosition(pos);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
    }

	private void treasureHuntButtonPressed(HWUGeneral.TreasureHuntRunningMode runningMode) {
		mCondition = mRadioCondition.getCheckedRadioButtonId();
		mUserID = mEditUserID.getText().toString();
		mLogFileName = "";

		// check that feedback type and route were selected and user ID has been entered
		if ((mCondition != -1) && (selectedRoute != null) && (!mUserID.equals(""))) {
			presentTreasureHuntActivity(runningMode);
		} else {
			mSetupError.setVisibility(View.VISIBLE);
		}
	}


	private void presentTreasureHuntActivity(HWUGeneral.TreasureHuntRunningMode runningMode) {
		mSetupError.setVisibility(View.INVISIBLE);
		Intent intent = new Intent(getApplicationContext(), HWUTreasureHuntActivity.class);
		//Intent intent = new Intent(getApplicationContext(), HeartRateActivity.class);
		intent.putExtra(HWUGeneral.USER_ID, mUserID);
		mLogFileName += mUserID;
		// setting the selected feedback type
		if (mCondition == R.id.rbEmpathic) {
			intent.putExtra(HWUGeneral.CONDITION, HWUGeneral.EMPATHIC);
			mLogFileName += "_" + HWUGeneral.EMPATHIC;
		} else {
			intent.putExtra(HWUGeneral.CONDITION, HWUGeneral.NON_EMPATHIC);
			mLogFileName += "_" + HWUGeneral.NON_EMPATHIC;
		}
		intent.putExtra(HWUGeneral.ROUTE, selectedRoute.getName());
        mLogFileName += "_" + selectedRoute.getName();

		intent.putExtra(HWUGeneral.LOG_FILE, mLogFileName);
		intent.putExtra(HWUGeneral.RUNNING_MODE, runningMode);
		disableComponents();
		mBtnReconfigureApp.setLongClickable(true);
		startActivity(intent);
	}

	 	private void disableComponents() {
	 		// disable all the components
    		for (int i = 0; i <mRadioCondition.getChildCount(); i++)
    		{
    			mRadioCondition.getChildAt(i).setEnabled(false);
    		}
    		mRouteSpinner.setEnabled(false);
    		mEditUserID.setEnabled(false);
	 	}

	 	private void resetComponents() {
	 		mRadioCondition.clearCheck();
        	mRouteSpinner.setSelected(false);
        	mEditUserID.setText("");

    		// enable all the components
    		for (int i = 0; i <mRadioCondition.getChildCount(); i++)
    		{
    			mRadioCondition.getChildAt(i).setEnabled(true);
    		}
    		mRouteSpinner.setEnabled(true);
    		mEditUserID.setEnabled(true);

    		// Delete previous data from shared preferences
    		SharedPreferences setup = getSharedPreferences(HWUGeneral.SETUP_FILE, 0);
		    SharedPreferences.Editor setupEditor = setup.edit();
		    setupEditor.putInt(HWUGeneral.CONDITION, -1);
		    setupEditor.putInt(HWUGeneral.ROUTE, -1);
		    setupEditor.putString(HWUGeneral.USER_ID, "");
		    setupEditor.putString(HWUGeneral.LOG_FILE, "");
		    // Commit the edits!
		    setupEditor.commit();

    	    SharedPreferences huntData = getSharedPreferences(HWUGeneral.DATA_FILE, 0);
    	    SharedPreferences.Editor editor = huntData.edit();
    	    editor.putInt(HWUGeneral.TREASURE_HUNT_MODE, HWUGeneral.CLUE);
    	    editor.putInt(HWUGeneral.CURRENT_STEP, 0);
    	    editor.putInt(HWUGeneral.CURRENT_QUESTION, 0);
    	    editor.putInt(HWUGeneral.CURRENT_POINT, 0);
    	    editor.putInt(HWUGeneral.CORRECT_ANSWERS, 0);
    	    editor.putInt(HWUGeneral.TOTAL_TREASURE_CLICKS, 0);
    	    editor.putInt(HWUGeneral.TOTAL_CLICK_TREASURES, 0);
    	    editor.putInt(HWUGeneral.TOTAL_QUESTIONS, 0);
    	    editor.putInt(HWUGeneral.TOTAL_UNLOCKED, 0);
    	    editor.putInt(HWUGeneral.TOTAL_TREASURES, 0);
    	    editor.putInt(HWUGeneral.INTERVENTIONS, 0);
    	    editor.putInt(HWUGeneral.NI_POINTS, 0);
    	    editor.putFloat(HWUGeneral.BASELINE_HR, 0.0f);
    	    editor.putString(HWUGeneral.PROCEED_STRING, "");
    	    editor.putBoolean(HWUGeneral.PRIZE_Q_ANSWERED, false);
    	    //editor.putBoolean(HWUGeneral.MIGRATED, false);
    	    editor.putBoolean(HWUGeneral.AT_DESTINATION, false);

    	    // Commit the edits!
		    editor.commit();
	 	}

	 	@Override
		public void onResume() {
		   super.onResume();
		   // Restore preferences
	        SharedPreferences settings = getSharedPreferences(HWUGeneral.SETUP_FILE, 0);
	        mCondition = settings.getInt(HWUGeneral.CONDITION, -1);
//	        selectedRoute = fetchRoutes(). settings.getString(HWUGeneral.ROUTE, "");
	        mUserID = settings.getString(HWUGeneral.USER_ID, "");
	        mLogFileName = settings.getString(HWUGeneral.LOG_FILE, "");

	        //Log.d(getClass().getSimpleName(), "onResume " + mFeedbackType + " " + mRoute + " " + mUserID);

	        // setup data exists
	        if (mCondition != -1) {
		        mRadioCondition.check(mCondition);
//		        mRadioRoute.check(mRoute);
		        mEditUserID.setText(mUserID);
		        disableComponents();
	        }

//	        // check route 1 (for June 2015 experiment)
//	        mRadioRoute.check(R.id.rbRoute1);
//	        for (int i = 0; i <mRadioRoute.getChildCount(); i++)
//    		{
//    			mRadioRoute.getChildAt(i).setEnabled(false);
//    		}
		}

		@Override
		public void onPause() {
		   super.onPause();

		   	// Saving the data in shared preferences
		    SharedPreferences settings = getSharedPreferences(HWUGeneral.SETUP_FILE, 0);
		    SharedPreferences.Editor editor = settings.edit();
		    editor.putInt(HWUGeneral.CONDITION, mCondition);
//		    editor.putString(HWUGeneral.ROUTE, selectedRoute.getName());
		    editor.putString(HWUGeneral.USER_ID, mUserID);
		    editor.putString(HWUGeneral.LOG_FILE, mLogFileName);
		    //Log.d(getClass().getSimpleName(), "onPause " + mFeedbackType + " " + mRoute + " " + mUserID + " " + mMachineIP);

		    // Commit the edits!
		    editor.commit();
		}

}