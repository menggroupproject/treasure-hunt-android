package hw.emote.hwutreasurehunt;

import android.content.Context;
import android.os.AsyncTask;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import hw.emote.routeparser.Route;
import hw.emote.routeparser.XMLRouteReader;

/**
 * Created by alasdairlaw on 03/03/2016.
 */

public class RouteService {
    public interface RouteDownloadDelegate {
        public void routesFetched(ArrayList<Route> routes);
    }

    private static RouteService mInstance = null;

    private RouteDownloadDelegate delegate;
    private Context context;
    private ArrayList<Route> routes;

    public static RouteService getInstance() {
        if (mInstance == null) {
            mInstance = new RouteService();
        }
        return mInstance;
    }

    public void setupService(RouteDownloadDelegate delegate, Context context) {
        this.delegate = delegate;
        this.context = context;
    }

    public void resetRoutes() {
        context.deleteFile("routes.xml");
    }

    public void fetchRoutes() {
        try {
            FileInputStream fis = context.openFileInput("routes.xml");
            XMLRouteReader routeReader = new XMLRouteReader(fis);
            routes = routeReader.getRoutes();
            fis.close();
            delegate.routesFetched(routes);
        } catch (FileNotFoundException e) {
            FetchRoutesTask task = new FetchRoutesTask();
            task.execute();
        } catch (java.net.MalformedURLException e) {

        } catch (org.json.JSONException e) {

        } catch (java.io.IOException e) {

        }
    }

    public Route routeWithName(String name) {
        for (Route route : routes) {
            if (name.equals(route.getName())) {
                return route;
            }
        }
        return null;
    }

    private class FetchRoutesTask extends AsyncTask<Void, Void, ArrayList<Route>> {
        @Override
        protected ArrayList<Route> doInBackground(Void... params) {
            try {
                URL url = new URL("http://protected-lake-87247.herokuapp.com/routes");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                int response = conn.getResponseCode();
                InputStream is = conn.getInputStream();
                XMLRouteReader routeReader = new XMLRouteReader(is);

                FileOutputStream fos = context.openFileOutput("routes.xml", Context.MODE_PRIVATE);
                fos.write(routeReader.getXML().getBytes());
                fos.close();

                routes = routeReader.getRoutes();
                return routes;
            } catch (Exception e) {
                System.out.println(e.getLocalizedMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Route> routes) {
            routes = routes;
            delegate.routesFetched(routes);
        }
    }

}
