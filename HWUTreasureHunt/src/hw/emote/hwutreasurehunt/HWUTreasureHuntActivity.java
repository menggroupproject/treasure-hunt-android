package hw.emote.hwutreasurehunt;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Observer;

import org.json.JSONException;
import hw.emote.hwutreasurehunt.hr.BandDataMonitor;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import hw.emote.dialogsystem.AndroidDialogInterface;
import hw.emote.dialogsystem.AndroidDialogProvider;
import hw.emote.dialogsystem.DialogSystem;
import hw.emote.eatreasurehunt.R;
import hw.emote.feedbackparser.FeedbackItem;
import hw.emote.feedbackparser.FeedbackTemplate;
import hw.emote.feedbackparser.XMLFeedbacksReader;
import hw.emote.hwutreasurehunt.affect.Arousal;
import hw.emote.hwutreasurehunt.affect.Valence;
import hw.emote.hwutreasurehunt.location.BearingService;
import hw.emote.hwutreasurehunt.location.GeofenceHandler;
import hw.emote.hwutreasurehunt.location.GeofenceService;
import hw.emote.hwutreasurehunt.location.LocationService;
import hw.emote.hwutreasurehunt.location.UserTracking;
import hw.emote.routeparser.Point;
import hw.emote.routeparser.Question;
import hw.emote.routeparser.Route;
import hw.emote.routeparser.Step;
import hw.emote.routeparser.XMLRouteReader;
import uk.ac.hw.lirec.emys3d.Emys3DActivity;
import uk.ac.hw.lirec.emys3d.EmysModel.Emotion;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.hardware.GeomagneticField;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.*;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.*;
import com.facebook.share.model.*;
import com.facebook.share.widget.ShareDialog;
import com.facebook.share.Sharer;
import com.facebook.share.Sharer.*;

import java.util.Observable;
import hw.emote.hwutreasurehunt.sensor.*;
import static hw.emote.hwutreasurehunt.sensor.SensorValue.Type.*;
/**
 * @author Mei Yii Lim
 *
 * This is the main activity for the EATreasureHunt application. 
 * 
 * It implements the OnTouchListener which controls the drag and zoom functionalities of the Map,
 * Android Dialog Provider for the Dialog system and OnInitListener to detect the end of TTS utterance
 * LocationListener detects the location changes
 */

public class HWUTreasureHuntActivity extends Emys3DActivity implements 
		OnTouchListener, 
		AndroidDialogProvider, 
		OnInitListener,
        Observer,
		DownloadImage.DownloadImageDelegate {
    
	private static final String TAG = "HWUTreasureHuntActivity";
	private static final double BEARING_THRESHOLD = 30.0;
	private static final float INIT_DISTANCE = 1000.0f;
	private static final float WRONG_DIRECTION_THRESHOLD = 25;
	private static final float PASSED_DESTINATION_THRESHOLD = 25;
	private static final long SECOND_IN_MILI = 1000;
	private static final long WRONG_DIR_TIME_THRESHOLD = 10*SECOND_IN_MILI; // 10 seconds
	private static final long CLUE_IDLE_THRESHOLD = 45*SECOND_IN_MILI; // 45 seconds
	private static final long QUESTION_IDLE_THRESHOLD = 30*SECOND_IN_MILI; // 30 seconds
	private static final long PUMPKIN_THRESHOLD = 10*SECOND_IN_MILI; // 10 seconds
    private static final String SCORE_WEBPAGE = "http://www2.macs.hw.ac.uk/~lm258/th/score.php";
	
	// Intent
	private Intent mIntent;
	
	private String mUserID;
	private String mSelectedCondition;
	private String mSelectedRoute;

	private HWUGeneral.TreasureHuntRunningMode mRunningMode;
	
	// Layout components
	private TextView mTxtInstructionLink;
	private TextView mTxtSolutionLink;
	private ImageView mTreasureMap;
	private ImageView mTreasure;
	private ImageView mCompass;
	private ScrollView mScroll;
	private TextView mTxtSubtitles;
	private ImageView mSymbol;
	private Spinner spAnswers;
	private Button mBtnProceed;
	private Button mBtnPlayAgain;
	private Button mBtnGetBaseline;
	private TextView mTxtError;	
	
	// Translate and zoom related variables
	private static final float MIN_ZOOM = 0.5f;
	private static final float MAX_ZOOM = 2.0f;
	private Matrix mMatrix = new Matrix();
	private Matrix mSavedMatrix = new Matrix();
	//private Matrix mScaleMatrix = new Matrix();
	//private Matrix mSavedScaleMatrix = new Matrix();
	private PointF mStartPoint = new PointF();
	private PointF mMidPoint = new PointF();
	private float mOldDist = 1f;
	private float mScale = 1f;
	private static final int NONE = 0;
	private static final int DRAG = 1;
	private static final int ZOOM = 2;
	private int mode = NONE;
	
	//dialog system
	private DialogSystem mDialogSystem;
	private AndroidDialogInterface mDi = new AndroidDialogInterface(this,this);
	
	// Feedback file and related variables
	private ArrayList<FeedbackTemplate> mFeedbacks;
	private FeedbackTemplate mFeedback;
	
	// Steps file and related variables
	private Step mStep;
	private ArrayList<Point> mPoints;
	private Point mPoint;
	private ArrayList<Question> mQuestions;
	private String mClue;
	private String mSolution;
	private Route mRoute;
	private String mAnswer;
	private static int mCurrentStep = 0;
	private static int mCurrentPoint = 0;
	private static int mCurrentQuestion = 0;
	private static int mCorrectAnswers = 0;
	private static int mCorrectAnswersAll = 0;
	private static int mInterventions = 0;
	private static String mProceedString = "";
	private int mTreasureHuntMode;	
	private String mTTSText;
	private int mResumeStep;
	private boolean mLoadMap;
	private Integer[] mImageIcons;
	
	// Clue dialog box components
	private Dialog mClueDialog;
	private TextView mTxtClueInstructions;
	private Button mBtnCloseClue;
	
	// Clue dialog box components
	private Dialog mSolutionDialog;
	private TextView mTxtSolution;
	private Button mBtnCloseSolution;
	
	// External Storage Log 
	private LogManager mEALogManager;
	private String mLogFileName;

	// the compass picture angle 
 	private float mCurrentDegree;
 	private float mAzimuth;
 	private float mDeclination;
 	private float mBearing;
	
	//used to play sound emblems, if called in the dialog script
	private MediaPlayer mMediaPlayer;
  
    // Geofence
    private GeofenceHandler mGeofenceHandler; 
    private ArrayList<Point> mGeofencePoints;
    
    /*private TextView mTxtBearing;
    private TextView mTxtUserBearing;
    private TextView mTxtHeartRate;
    private TextView mTxtNextDestination;*/

    // user tracking variables
    private long mClueStartTime;
    private long mQuestionStartTime;
    private long mWrongDirStartTime;
    private boolean mOnTheMove; // whether the user has moved
    private boolean mAnsweredQuestion; // where the user has answered the question
    private boolean mClueStarted; // whether the clue has started
    private boolean mQuestionStarted; // whether the question has started
    private boolean mAtDestination; // whether the user is at destination
	private boolean mWrongDirection; // whether the user is going in the wrong direction
	private boolean mGotThere; // whether the user has pressed got there
	private Location mWrongLocStart; // location where the user starts going astray
	private float mDistanceToDestination;
	
    // timer for recording heart rate
    private Timer mTimer;
	private TimerTask mTask;
	
	// emotion variables
	private Arousal mArousal;
	private Valence mValence;
	
	// user tracking variables
	private UserTracking mUserTracking;
	
	// getting the user's baseline
	private boolean mBaseline = false;
    
	// pumpkin for dual tasks 
	private ImageView mMapTreasure;
	private static int mTreasureClicks = 0;
	private static int mTotalTreasures = 0;
	private long mTreasureStartTime;
	private boolean mTreasureVisible = false;
	private DownloadImage imageDownloader = new DownloadImage();
	
	// whether the app has been onPause and resumed
	private boolean mResumed = false;

    //FaceBook integration variables
    private boolean mLoggedIn = false;
    private LoginButton mLoginButton; 
    private AccessTokenTracker accessTokenTracker;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    //Abstraction for observing our sensor input
    private BandDataMonitor sensors;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Initialize facebook SDK and create callbackManager
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        //Create our sensor observer then start observing
        sensors = new BandDataMonitor( getApplicationContext() );
        sensors.addObserver( this );
        sensors.handleReset( this );
        sensors.initialize( this );
		
		// Retrieve information passed over from the setup activity
		mIntent = getIntent();
		mRunningMode = (HWUGeneral.TreasureHuntRunningMode) mIntent.getSerializableExtra(HWUGeneral.RUNNING_MODE);
		mUserID = mIntent.getStringExtra(HWUGeneral.USER_ID);
		mSelectedCondition = mIntent.getStringExtra(HWUGeneral.CONDITION);
		mSelectedRoute = mIntent.getStringExtra(HWUGeneral.ROUTE);
		mLogFileName = mIntent.getStringExtra(HWUGeneral.LOG_FILE);
		//Toast.makeText(getApplicationContext(), "condition :: "+ mSelectedCondition, Toast.LENGTH_SHORT).show();
		//Log.d(TAG,"user id " + mUserID + " feedback " + mSelectedFeedbackType + " route " + mSelectedRoute );
		
		// Initialise the layout components
		setContentView(R.layout.activity_eatreasure_hunt);
		mTxtInstructionLink = (TextView) findViewById(R.id.txtInstructionLink);
		mTxtSolutionLink = (TextView) findViewById(R.id.txtSolutionLink);
		mTreasureMap = (ImageView) findViewById(R.id.treasureMap); 
		mMapTreasure = (ImageView) findViewById(R.id.mapTreasure);
		
        imageDownloader.delegate = this;

        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback( callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText( getApplicationContext(), "Shared to facebook.", Toast.LENGTH_SHORT).show();
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException e) {
                LoginManager.getInstance().logOut();
            }
        });

    	mTreasureMap.setOnTouchListener(this);       
    	mTreasureMap.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
    	    @Override
    	    public void onGlobalLayout() {
    	       // center the map upon load
    	       if (!mLoadMap) {
	    	       int width = mTreasureMap.getWidth();  
	    	       //Toast.makeText(getApplicationContext(), "map width :: "+ width + " scale width :: " + mMapScale.getWidth(), Toast.LENGTH_LONG).show();
	    	       mMatrix.set(mSavedMatrix);
	    	       //mScaleMatrix.set(mSavedScaleMatrix);
				   mMatrix.postTranslate(mStartPoint.x - width/3, mStartPoint.y);
				   mTreasureMap.setImageMatrix(mMatrix);
				   //mMapScale.setImageMatrix(mScaleMatrix);
				   mLoadMap = true; // map has been loaded
    	       }
    	    }
    	});
    	   	
    	// initialise compass variables
    	mCompass = (ImageView) findViewById(R.id.compass);
    	mCurrentDegree = Float.NaN;
    	mAzimuth = Float.NaN;
    	mDeclination = Float.NaN;
    	
    	mScroll = (ScrollView) findViewById(R.id.clueScroll);    
    	mScroll.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
	           @Override
	           public void onGlobalLayout() {
	               // Ready, move up
	               mScroll.fullScroll(View.FOCUS_UP);
	           }
	       });
    	mTxtSubtitles = (TextView) findViewById(R.id.txtSubtitles);
    	mSymbol = (ImageView) findViewById(R.id.symbol);
    	spAnswers = (Spinner) findViewById(R.id.spAnswers);
    	mBtnProceed = (Button) findViewById(R.id.btnProceed);
    	mBtnPlayAgain = (Button) findViewById(R.id.btnPlayAgain);
    	mBtnGetBaseline = (Button) findViewById(R.id.btnGetBaseline);
    	mTxtError = (TextView) findViewById(R.id.txtError);
    	
    	// disable buttons to prevent user from clicking before Emys loads
    	disableButtons();
    	
    	// dialog system 
        mDialogSystem = new DialogSystem(mDi);
        
        // initialise the data
        initData();
     
        // Detect if the user clicks on the pumpkin
        mMapTreasure.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// increase clicks count
				mTreasureClicks++;
				
				// remove the pumpkin
				mMapTreasure.setVisibility(View.INVISIBLE);
				mEALogManager.writeToLogFile("******************** User clicked on treasures *********************\n");
				//Toast.makeText(getApplicationContext(), "Clicks : " + mTreasureClicks + " Total : " + mTotalTreasures, Toast.LENGTH_SHORT).show();
			}
		});
        
        // Link to Instruction Screen
        mTxtInstructionLink.setOnClickListener(new View.OnClickListener() {        	
        	
            @Override
			public void onClick(View view) {   
            	displayInstructionsDialog();
            }
        });
        
        // Link to Solution Screen
        mTxtSolutionLink.setOnClickListener(new View.OnClickListener() {        	
        	
            @Override
			public void onClick(View view) {  
            	// user looks at the solution
            	mTreasureHuntMode = HWUGeneral.SOLUTION;
            	// delete all geofences except the last clue geofence for this step
            	getClueGeofence();
            	// penalise the user
            	mValence.increaseIntervention();
    			mEALogManager.writeToLogFile("************************ User clicked for solution *************************\n"); 
    			// display the dialog
            	displaySolutionDialog();
            }
        });
        
        // Proceed button Click Event - Display the next clue
        mBtnProceed.setOnClickListener(new View.OnClickListener() {
 
            @Override
			public void onClick(View view) {
            	mSymbol.setVisibility(View.INVISIBLE);            	
            	if (mTreasureHuntMode == HWUGeneral.CLUE) {        			
	            	// introduction step 
	            	if(mCurrentStep == 0){	            			
	            		mBtnProceed.setText(getResources().getText(R.string.next));	  
	            	// locating starting point and clue steps
	            	} else if (mCurrentStep >= 1) {
	            		mBtnProceed.setText(getResources().getText(R.string.got_there));
	            	} 
	            	displayClue();
            	} else if (mTreasureHuntMode == HWUGeneral.QUESTION) {
            		// ********* User is not at destination *************
            		if (!mAtDestination && mRunningMode != HWUGeneral.TreasureHuntRunningMode.DEMONSTRATION) {
            			intervene(HWUGeneral.NOT_AT_DESTINATION, HWUGeneral.MOVING);			
            		} else {
            			// user clicked on Got There
            			mGotThere = true;
            			
            			// disable solution link when user has arrived at the destination
            			mTxtSolutionLink.setVisibility(View.INVISIBLE);
            			
            			// clear geofences lists
            			clearGeofenceLists();
            			
            			// if an answer is required
                		if (mQuestions.get(0).getAnswers().size() > 0) {
                			mBtnProceed.setText(getResources().getText(R.string.answer_question));
                		// paper task - no answer required
                		} else {
                			mBtnProceed.setText(getResources().getText(R.string.done));            			
                		}
            			displayQuestion();
            		}
            	} else if (mTreasureHuntMode == HWUGeneral.ANSWER) {
            		// user is answering the question
            		//mQuestionStarted = false;
            		mAnsweredQuestion = true;
            		if (mAnswer.equals(getResources().getText(R.string.select_answer).toString())) {
            			mTxtError.setVisibility(View.VISIBLE);            			
            		} else {
            			mTxtError.setVisibility(View.INVISIBLE);
	            		mBtnProceed.setText(getResources().getText(R.string.next));
	            		displayFeedback();
            		}
            	} else if (mTreasureHuntMode == HWUGeneral.SOLUTION) {
            		if(mResumed) {
            			mBtnProceed.setText(getResources().getText(R.string.got_there));
            			displayClue();
            		} else if (!mAtDestination) {
            			intervene(HWUGeneral.NOT_AT_DESTINATION, HWUGeneral.MOVING);			
            		}
            	}
            	
            	// the app has been resumed
            	mResumed = false;
            	
            	// Allow TTS replay
            	mBtnPlayAgain.setVisibility(View.VISIBLE);
            	//Log.d(TAG,"mode " + treasureHuntMode + " " + currentStep + " " + currentQuestion);
            }
        });
        
        // Proceed button Click Event - Display the next clue
        mBtnPlayAgain.setOnClickListener(new View.OnClickListener() {
 
            @Override
			public void onClick(View view) {
            	mEALogManager.writeToLogFile("******************** Play Again pressed **********************\n");
            	speakText(mTTSText);
            	disableButtons();
            }
        });
        
    	// compass bearing
    	mBearing = Float.NaN;
    	
    	// user locations
    	mUserTracking = new UserTracking();
    	mWrongLocStart = new Location("Wrong");
    	mWrongLocStart.setLatitude(0.0);
    	mWrongLocStart.setLongitude(0.0);
    	mWrongDirection = false;
    	mDistanceToDestination = INIT_DISTANCE;
    	
        // start the location service - only once as it will be running continuously in the 
        // background until the app is destroyed
        startLocationService();
        //registerReceiver(testlocationReceiver, new IntentFilter(TEST_BROADCAST_LOCATION));
        
        // Geofence handler for adding and removing geofences
        mGeofenceHandler = new GeofenceHandler(this);
        // start the geofence service
        startGeofenceService();
        mPoints = new ArrayList<Point>();
        mPoint = null;
        mGeofencePoints = new ArrayList<Point>();
        mAtDestination = false;    
        mGotThere = false;
        
        // resets the PCC connection
        //mHeartRateMonitor = new HeartRateMonitor(this);
	    //mHeartRateMonitor.handleReset();
	    //registerReceiver(hrmReceiver, new IntentFilter(HeartRateMonitor.BROADCAST_HEARTRATE));
        sensors.handleReset( this );
        //mHeartRate = "";  
        mArousal = new Arousal();
        mValence = new Valence();
        
	    //********** GPS button Click Event - Record GPS coordinates **********
    	// intent to broadcast the location changes
        /*Button mBtnGPSCoordinates = (Button) findViewById(R.id.btnGPSCoordinates);
        mBtnGPSCoordinates.setOnClickListener(new View.OnClickListener() {
 
            @Override
			public void onClick(View view) {
            	if ((mUserTracking != null) && (mPoint != null) && (mEALogManager != null)) {
            		mEALogManager.writeAtPointToHRFile(mUserTracking.getCurrentLocation(), mPoint.getName(), mArousal.getHeartRate());
            	}
            }
        });*/
       
        /*mTxtBearing = (TextView) findViewById(R.id.txtBearing);
        mTxtUserBearing = (TextView) findViewById(R.id.txtUserBearing);
        mTxtHeartRate = (TextView) findViewById(R.id.txtHeartRate);
        mTxtNextDestination = (TextView) findViewById(R.id.txtNextDestination);*/
	}

    void showFinishDialog() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                case DialogInterface.BUTTON_POSITIVE:
					sensors.finish();
					facebookShare();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
					sensors.finish();
                    break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder( HWUTreasureHuntActivity.this );
        builder.setMessage( String.format( this.getString(R.string.complete_dialog_message) , mCorrectAnswersAll ) )
            .setPositiveButton( R.string.complete_dialog_share, dialogClickListener)
            .setNegativeButton( R.string.complete_dialog_close, dialogClickListener).show();
    }

    /**
     * This function is called when updated band information
     * is received through an event. This could be a device
     * status change or a heart rate reading.
    */
    public void update( Observable o, Object arg ) {

        //Handle events from Band devices IE errors etc
        if( arg != null && arg.getClass().equals( SensorStatus.class ) ) {
            SensorStatus.Status status = ((SensorStatus)arg).getStatus();
            final String message = ((SensorStatus)arg).getMessage();
            switch( status ) {
                default:
                    this.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    });
                break;
            }
        }

        //Event called when a sensor value is received
        if( arg != null && arg.getClass().equals( SensorValue.class ) ) {
            SensorValue.Type status = ((SensorValue)arg).getType();
            Object[] values = ((SensorValue)arg).getValues();
            switch( status ) {
                case HEART_RATE:
                    float heartRate     = (Float)values[0];

                    mArousal.setHeartRate(heartRate);

                    runOnUiThread( new Runnable() {
                        @Override
                        public void run() {
                            //mTxtHeartRate.setText("Heart Rate: " + mHeartRate);
                            //TODO need to move this to timertask
                            if( mClueStarted && !mOnTheMove ) {
                                long clueTimeDiff = SystemClock.elapsedRealtime() - mClueStartTime;
                                
                                if (clueTimeDiff > CLUE_IDLE_THRESHOLD) {
                                    // ********* User is confused ************
                                    intervene(HWUGeneral.CONFUSED, HWUGeneral.STATIONARY);  
                                    mClueStarted = false;
                                }                               
                            }
                            if( mQuestionStarted && !mAnsweredQuestion ) {
                                long questionTimeDiff = SystemClock.elapsedRealtime() - mQuestionStartTime;
                                
                                if (questionTimeDiff > QUESTION_IDLE_THRESHOLD) {
                                    // ********* User can't find the clue **********
                                    intervene(HWUGeneral.CLUE_NOT_FOUND, HWUGeneral.STATIONARY);    
                                    mQuestionStarted = false;
                                }                               
                            }
                            
                            // Toast.makeText(getApplicationContext(), "Computed Heart Rate : " + mArousal.getHeartRate(), Toast.LENGTH_SHORT).show();
                            if (mBaseline && (mTreasureHuntMode == HWUGeneral.BASELINE)) {
                                Toast.makeText(getApplicationContext(), getResources().getText(R.string.baseline_recorded).toString(), Toast.LENGTH_SHORT).show();
                                // Emys speaking
                                emysSpeak(Emotion.NEUTRAL, getResources().getText(R.string.baseline_recorded).toString() + " " + getResources().getText(R.string.start_instruction).toString());
                                //Toast.makeText(getApplicationContext(), "Baseline : " + mArousal.getBaseline(), Toast.LENGTH_SHORT).show();
                                mTreasureHuntMode = HWUGeneral.CLUE;
                                enableButtons();
                            }
                            
                            // display pumpkin for dual task only for 10 seconds
                            if (mTreasureVisible) {
                                long pumpkinTimeDiff = SystemClock.elapsedRealtime() - mTreasureStartTime;
                                
                                if (pumpkinTimeDiff > PUMPKIN_THRESHOLD) {
                                    mTreasureVisible = false;
                                    // hide the pumpkin
                                    mMapTreasure.setVisibility(View.INVISIBLE);
                                }
                            }
                        }
                    });
                break;
            }
        }
    }

	/**
     * Add override for onActivityResult so the event can be passed
     * to the facebook callbackManager.
    */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,
                resultCode, data);
    }

    /**
     * Function used for sharing a users score etc on facebook after completion
    */
    public void facebookShare() {
        ShareLinkContent content = new ShareLinkContent.Builder()
        .setContentTitle("Heriot-Watt Treasure Hunt")
        .setContentUrl(Uri.parse( SCORE_WEBPAGE + "?correct=" + mCorrectAnswersAll))
        .build();
        shareDialog.show( content );
    }

    //@Override
    public void onStop() {
        super.onStop();

        LoginManager.getInstance().logOut();
        System.out.println( "Facebook Logged Out." );
    }


	
	/**
	 * Return the intervention string for a particular scenario
	 */
	public void intervene(String reason, String state) {	
		// intervene only from step 2 when the first clue has been presented and not on the last step
		if ((mCurrentStep > 1) && (mCurrentStep < mRoute.getSteps().size())) {
			// Increase the number of interventions
			mInterventions++;
			mValence.increaseIntervention();
			
			int strInterventionID = getStringResourceId("neutral_" + reason);
			//Emotion emotion = Emotion.NEUTRAL;
			String arousal = mArousal.getMovingArousal();
			if (state == HWUGeneral.STATIONARY){ 
				arousal = mArousal.getStaticArousal();
			}			
			String valence = mValence.getValence();
			
			// Intervention string of empathic agent
			if (isAgentWithEmpathy()){
				// happy/excited user
				if ((arousal == Arousal.HIGH) && (valence == Valence.POSITIVE)) {
					strInterventionID = getStringResourceId("happy_" + reason);
				// frustrated/angry user
				} else if ((arousal == Arousal.HIGH) && (valence == Valence.NEGATIVE)) {
					strInterventionID = getStringResourceId("frustrated_" + reason);
				// sad/bored user
				} else if ((arousal == Arousal.LOW) && (valence == Valence.NEGATIVE)) {
					strInterventionID = getStringResourceId("bored_" + reason);
				}            				
			} 
			String strIntervention = getResources().getText(strInterventionID).toString();
			
			// Emys speaking
			emysIntervene(Emotion.NEUTRAL, strIntervention);
			
			// Logging details
			mEALogManager.writeToLogFile("Arousal: " + arousal + ", Valence: " + valence + "\n");
			mEALogManager.writeToLogFile("Correct answers: " + mValence.getCorrect() + ", Total Q: " + mValence.getTotalQ() + "\n");
			mEALogManager.writeToLogFile("Unlocked treasures: " + mValence.getUnlocked() + ", Total Treasures: " + mValence.getTotalTreasures() + "\n");
			mEALogManager.writeToLogFile("Interventions: " + mValence.getInterventions() + ", Total Points: " + mValence.getNIPoints() + "\n");
			mEALogManager.writeToLogFile("Treasures clicked: " + mTreasureClicks + ", Total click treasures: " + mTotalTreasures + "\n");
			
			mEALogManager.writeToLogFile(strIntervention + "\n"); 
			
			// Enable the solution link after max number of interventions reached
			if (mInterventions == HWUGeneral.INTERVENTION_NO) {
				mTxtSolutionLink.setVisibility(View.VISIBLE);
			}
		}
	}
        
    @Override
	public void onResume() {      
	    super.onResume();
		
	    // Log Manager
	 	mEALogManager = new LogManager();
	 	if (mEALogManager.checkStorageStatus()) {
	 		mEALogManager.createLogFile(mLogFileName + ".txt");
	 		mEALogManager.createHRFile(mLogFileName + ".txt");
	 	}
    	
	    restoreSharedPreferences();
       
        // remember the resume step to prevent continuous decrease of step due to the user pausing and resuming the app
        mResumeStep = mCurrentStep;
        // map has not been loaded
        mLoadMap = false;
        
        // when in question and answer modes, retrieve the question of the previous step without decreasing the currentStep
        if (mTreasureHuntMode == HWUGeneral.ANSWER || (mTreasureHuntMode == HWUGeneral.QUESTION && mCurrentQuestion > 0)) {
        	mStep = mRoute.getSteps().get(mCurrentStep-1);
        	mQuestions = mStep.getQuestions();      
        // when in clue or first question mode, decrease the currentStep so that the previous clue before onPause 
        // can be displayed, this is necessary because currentStep was increased before onPause in displayClue()
        // not necessary when onPause occurred during feedback and steps where answer is not required as we will move 
        // to the next step onResume
        } else if ((mCurrentStep > 0 && !mProceedString.equals(getResources().getText(R.string.next).toString()) && 
        		!mProceedString.equals(getResources().getText(R.string.done).toString())) ||
        		mCurrentStep == 1) {      	
        	--mCurrentStep;
        }         
        
	    // If not already in question mode, remain in clue mode
	    if (mTreasureHuntMode == HWUGeneral.QUESTION && mCurrentQuestion == 0) {
	    	mTreasureHuntMode = HWUGeneral.CLUE;	    	
	    // if waiting for answer, re-present the question
	    } else if (mTreasureHuntMode == HWUGeneral.ANSWER) {
	    	mTreasureHuntMode = HWUGeneral.QUESTION;
	    }
	    
	    // the hunt already started
    	if (mCurrentStep > 0){
    		setSubtitle(R.string.continue_instruction);
    		mBtnProceed.setText(R.string.continue_hunt);
    		spAnswers.setVisibility(View.INVISIBLE);
    	} else if (!mBaseline){
    		setSubtitle(R.string.baseline_instruction);
      	} else {
      		setSubtitle(R.string.start_instruction);
    	}
    	
    	// Register ttsReceiver
    	this.registerReceiver(ttsReceiver, new IntentFilter("hw.emote.eatreasurehunt.tts"));
    	
    	// start the bearing service
	    startBearingService();	 
	    
	    // clear geofences lists
	    clearGeofenceLists();
	    if (mCurrentPoint > 0) {
	    	mCurrentPoint--;
	    }
	    
	    // initialise user activities variables
	    mOnTheMove = false;
	    mClueStarted = false;
	    mQuestionStarted = false;
	    mAnsweredQuestion = false;
	    
	    
	    // get the user's baseline
    	if (!mBaseline && mRunningMode != HWUGeneral.TreasureHuntRunningMode.DEMONSTRATION) {
	        mBtnGetBaseline.setVisibility(View.VISIBLE);
	        mBtnGetBaseline.setOnClickListener(new View.OnClickListener() {
	        	
	        	@Override
	        	public void onClick(View view) {
	        		mTreasureHuntMode = HWUGeneral.BASELINE;
	        		mBtnGetBaseline.setEnabled(false);	
	        		emysSpeak(Emotion.NEUTRAL, getResources().getText(R.string.baseline_recording).toString());
	        	}
	        });
    	}
	    
	    // start the hr recording timer
	    mTask = new TimerTask() {
	    	int baselineIndex = 0;
	    	float baseline = 200;
			//long totalBaseline = 0;
            @Override
            public void run() {    
            	// TODO
            	/*if (mClueStarted && !mOnTheMove) {
            		long clueTimeDiff = SystemClock.elapsedRealtime() - mClueStartTime;
            		
            		if (clueTimeDiff > CLUE_IDLE_THRESHOLD) {
            			// TODO User is confused
            			Toast.makeText(getApplicationContext(), "Start moving", Toast.LENGTH_SHORT).show();
            			mClueStarted = false;
            		}                    			
            	}
            	if (mQuestionStarted && !mAnsweredQuestion) {
            		long questionTimeDiff = SystemClock.elapsedRealtime() - mQuestionStartTime;
            		
            		if (questionTimeDiff > QUESTION_IDLE_THRESHOLD) {
            			// TODO User can't find the clue
            			Toast.makeText(getApplicationContext(), "Read instructions", Toast.LENGTH_SHORT).show();
            			mQuestionStarted = false;
            		}                    			
            	}*/
            	
            	// In baseline recording mode
            	if (mTreasureHuntMode == HWUGeneral.BASELINE) {
            		if (baselineIndex < HWUGeneral.HR_RECORD_TIME) {
            			baselineIndex += 1;
            			if (mArousal.getHeartRate() < baseline) {
            				baseline = mArousal.getHeartRate();
            			}
            			Log.i(TAG, "baseline " + baseline + " index " + baselineIndex);
            		} else {
            			mArousal.setBaseline(baseline);
    	        		mBaseline = true;
    	        		// log baseline
    	        		if ((mUserTracking != null) && (mEALogManager != null)) {
    	            		mEALogManager.writeToHRFile(mUserTracking.getCurrentLocation(), " Baseline HR: ", baseline);
    	            		mEALogManager.writeToLogFile("Baseline HR: " + baseline + "\n");
    	            	}
            		}
            	}             	
            	
               	if ((mUserTracking != null) && (mPoint != null) && (mEALogManager != null)) {
               		float aValue = mArousal.getHeartRate();
            		mEALogManager.writeToHRFile(mUserTracking.getCurrentLocation(), mPoint.getName(), aValue);
            		mEALogManager.writeToLogFile("Heart rate: " + aValue + "\n");
            	}
            }
        };

        mTimer = new Timer();
        mTimer.schedule(mTask, 5000, 1000); 
        
        // the app has just been resumed
        mResumed = true;
	}
	
	@Override
	public void onPause() {
	    super.onPause();
	    
	    // if the step has been decreased onResume and user has not moved to the next step, 
	    // restore the mCurrentStep to previous onPause step
	    if (mCurrentStep < mResumeStep) {
	    	mCurrentStep = mResumeStep;
	    }
	    
	    saveSharedPreferences();
	    
	    // stop hr recording timer
	    if (mTimer != null) {
	    	mTimer.cancel();
	    }
	    
	    mEALogManager.writeToLogFile("Total questions answered correctly: " + mValence.getCorrect() + "\n");
		mEALogManager.writeToLogFile("Total questions: " + mValence.getTotalQ() + "\n");
		mEALogManager.writeToLogFile("Total treasures unlocked: " + mValence.getUnlocked() + "\n");
		mEALogManager.writeToLogFile("Total treasures: " + mValence.getTotalTreasures() + "\n");
		mEALogManager.writeToLogFile("Total interventions: " + mValence.getInterventions() + "\n");
		mEALogManager.writeToLogFile("Total intervention points: " + mValence.getNIPoints() + "\n");
		mEALogManager.writeToLogFile("Total treasures clicked: " + mTreasureClicks + "\n");
		mEALogManager.writeToLogFile("Total click treasures: " + mTotalTreasures + "\n");
		
	    // Close the log file
	    mEALogManager.closeLogFile();
	    mEALogManager.closeHRFile();
	    
	    // Unregister ttsReceiver
	    this.unregisterReceiver(ttsReceiver);
	    
	    // stop the bearing service
	    stopBearingService();
	    mGeofenceHandler.removesGeofence();
	}
	
	@Override
	public void onDestroy() {
		stopAllServices();
		super.onDestroy();
		Log.d(TAG,"App destroyed");
	}
	
	/**
	 * Set the subtitles
	 * @param subtitle resource id
	 */
	private void setSubtitle(int subtitle) {
		mClue = getResources().getText(subtitle).toString();
		mTxtSubtitles.setText(subtitle);
	}
	
	/**
     * Clear geofences lists
     */
    private void clearGeofenceLists() {
    	// clear geofences ids 
		mPoints = new ArrayList<Point>();
		mGeofencePoints = new ArrayList<Point>();
    }
    
	/**
	 * Stop all services
	 */
	private void stopAllServices() {
		sensors.stop();
	    stopLocationService();
	    stopGeofenceService();
	}
	
	/**
	 * Prevent the user from closing the app during the hunt
	 */
	@Override
	public void onBackPressed() {
	   return;
	}
        	
	/** 
	 * ttsReceiver to receive broadcast on completion of tts utterance
	 */
	private BroadcastReceiver ttsReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// disable buttons and close the log files at the last step
			if (mCurrentStep == mRoute.getSteps().size()) {
				disableButtons();
				// Log the final step
				mEALogManager.writeToLogFile("End of treasure hunt! \n\n");
				
				mEALogManager.writeToLogFile("Total questions answered correctly: " + mValence.getCorrect() + "\n");
				mEALogManager.writeToLogFile("Total questions: " + mValence.getTotalQ() + "\n");
				mEALogManager.writeToLogFile("Total treasures unlocked: " + mValence.getUnlocked() + "\n");
				mEALogManager.writeToLogFile("Total treasures: " + mValence.getTotalTreasures() + "\n");
				mEALogManager.writeToLogFile("Total interventions: " + mValence.getInterventions() + "\n");
				mEALogManager.writeToLogFile("Total intervention points: " + mValence.getNIPoints() + "\n");
				mEALogManager.writeToLogFile("Total treasures clicked: " + mTreasureClicks + "\n");
				mEALogManager.writeToLogFile("Total click treasures: " + mTotalTreasures + "\n");
				
				// Close the log file
			    mEALogManager.closeLogFile();
			    mEALogManager.closeHRFile();
			    // stop all services - HRM, Geofence and Location
				stopAllServices();
			} else {
				// Enable the proceed and play again buttons
				enableButtons();
			}
			// Log.d(TAG, "ttsReceiver ");
		}
    };    
	
	/**
	 * Get the feedbacks and routes
	 */
	private void initData(){
		//Reading different feedback and steps from XML file
        try {
        	InputStream isFeedback = getAssets().open(HWUGeneral.FEEDBACKS_FILE);
        	// Reading the potential feedback
			XMLFeedbacksReader fbReader = new XMLFeedbacksReader(isFeedback);
			mFeedbacks = fbReader.getFeedback();
			for (int i = 0; i < mFeedbacks.size(); i++) {
				FeedbackTemplate fbTemplates = mFeedbacks.get(i);
				// use only the affective feedback (June 2015 experiment)
				if (fbTemplates.getName().equals(HWUGeneral.AFFECTIVE)){
					//Log.d(TAG,"fbTemplates " + mSelectedFeedbackType + " " + fbTemplates.getName());
					mFeedback = fbTemplates;
				}
			}
			// Reading the steps of the treasure hunt
			mRoute = RouteService.getInstance().routeWithName(mSelectedRoute);
			System.out.println(mRoute.getName());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	     					
	}
	
	/**
	 * Saving the data into shared preferences
	 */
	private void saveSharedPreferences() {
		  // Saving the data in shared preferences
	    SharedPreferences huntData = getSharedPreferences(HWUGeneral.DATA_FILE, 0);
	    SharedPreferences.Editor editor = huntData.edit();
	    editor.putInt(HWUGeneral.TREASURE_HUNT_MODE, mTreasureHuntMode);
	    editor.putInt(HWUGeneral.CURRENT_STEP, mCurrentStep);
	    editor.putInt(HWUGeneral.CURRENT_QUESTION, mCurrentQuestion);
	    editor.putInt(HWUGeneral.CURRENT_POINT, mCurrentPoint);
	    editor.putInt(HWUGeneral.CORRECT_ANSWERS, mCorrectAnswers);
	    editor.putInt(HWUGeneral.TOTAL_UNLOCKED, mValence.getUnlocked());
	    editor.putInt(HWUGeneral.TOTAL_TREASURES, mValence.getTotalTreasures());
	    editor.putInt(HWUGeneral.TOTAL_CORRECT, mValence.getCorrect());
	    editor.putInt(HWUGeneral.TOTAL_QUESTIONS, mValence.getTotalQ());
	    editor.putInt(HWUGeneral.TOTAL_TREASURE_CLICKS, mTreasureClicks);
	    editor.putInt(HWUGeneral.TOTAL_CLICK_TREASURES, mTotalTreasures);
	    editor.putInt(HWUGeneral.INTERVENTIONS, mValence.getInterventions());
	    editor.putInt(HWUGeneral.NI_POINTS, mValence.getNIPoints());
	    editor.putFloat(HWUGeneral.BASELINE_HR, mArousal.getBaseline());
	    //editor.putBoolean(HWUGeneral.TOTAL_ADDED, mTotalAdded);
	    editor.putString(HWUGeneral.PROCEED_STRING, mBtnProceed.getText().toString());
	    //editor.putBoolean(HWUGeneral.PRIZE_Q_ANSWERED, mPrizeQAnswered);
	    editor.putBoolean(HWUGeneral.AT_DESTINATION, mAtDestination);
	    Log.d(TAG,"onPause " + mTreasureHuntMode + " step " + mCurrentStep + " question " 
	    		+ mCurrentQuestion + " point " + mCurrentPoint + " correct " + mValence.getCorrect()
	    		+ " total Q " + mValence.getTotalQ() + " baseline " + mArousal.getBaseline() 
	    		+ " " + mBtnProceed.getText().toString());
	    
	    // Commit the edits!
	    editor.commit();
	}
	
	/**
	 * Retrieving data in shared preferences
	 */
	private void restoreSharedPreferences() {
		// Restore preferences
        SharedPreferences huntData = getSharedPreferences(HWUGeneral.DATA_FILE, 0);
        mTreasureHuntMode = huntData.getInt(HWUGeneral.TREASURE_HUNT_MODE, HWUGeneral.CLUE);
        mCurrentStep = huntData.getInt(HWUGeneral.CURRENT_STEP, 0);
        mCurrentQuestion = huntData.getInt(HWUGeneral.CURRENT_QUESTION, 0);
        mCurrentPoint = huntData.getInt(HWUGeneral.CURRENT_POINT, 0);
        mCorrectAnswers = huntData.getInt(HWUGeneral.CORRECT_ANSWERS, 0);
        mTreasureClicks = huntData.getInt(HWUGeneral.TOTAL_TREASURE_CLICKS, 0);
        mTotalTreasures = huntData.getInt(HWUGeneral.TOTAL_CLICK_TREASURES, 0);
        mValence.setCorrect(huntData.getInt(HWUGeneral.TOTAL_CORRECT, 0));
        mValence.setTotalQ(huntData.getInt(HWUGeneral.TOTAL_QUESTIONS, 0));
        mValence.setUnlocked(huntData.getInt(HWUGeneral.TOTAL_UNLOCKED, 0));
        mValence.setTotalTreasures(huntData.getInt(HWUGeneral.TOTAL_TREASURES, 0));
        mValence.setInterventions(huntData.getInt(HWUGeneral.INTERVENTIONS, 0));
        mValence.setNIPoints(huntData.getInt(HWUGeneral.NI_POINTS, 0));
        mArousal.setBaseline(huntData.getFloat(HWUGeneral.BASELINE_HR, 0.0f));
        mProceedString = huntData.getString(HWUGeneral.PROCEED_STRING, getResources().getText(R.string.next).toString());
        //mPrizeQAnswered = huntData.getBoolean(HWUGeneral.PRIZE_Q_ANSWERED, false);
        mAtDestination = huntData.getBoolean(HWUGeneral.AT_DESTINATION, false);
        Log.d(TAG,"onResume " + mTreasureHuntMode + " step " + mCurrentStep + " question " 
        		+ mCurrentQuestion + " point " + mCurrentPoint + " " + " correct " + mValence.getCorrect() 
        		+ " total Q " + mValence.getTotalQ() + " baseline " + mArousal.getBaseline() 
        		+ " mProceedString " + mProceedString);
	}
	
	/**
	 * Display the extra prize dialog
	 */
	/*private void displayPrizeDialog() {			
    	
		mDialog = new Dialog(this); 
    	
    	//tell the Dialog to use the dialog_extra_prize.xml as it's layout description
       	mDialog.setContentView(R.layout.dialog_extra_prize);
        mDialog.setTitle(R.string.extra_prize_on_offer);
        
        // get the components
        mSpQ1Answers = (Spinner) mDialog.findViewById(R.id.spQ1Answers);
        mSpQ2Answers = (Spinner) mDialog.findViewById(R.id.spQ2Answers);
        mTxtDialogError = (TextView) mDialog.findViewById(R.id.txtDialogError);
    	mBtnSubmit = (Button) mDialog.findViewById(R.id.btnSubmit);
       	mBtnClose = (Button) mDialog.findViewById(R.id.btnClose);	         	
        
        // if the user already answered the questions
        if (mPrizeQAnswered == true) {
        	mSpQ1Answers.setEnabled(false);
   			mSpQ2Answers.setEnabled(false);
   			mBtnSubmit.setEnabled(false);
   			mTxtDialogError.setText(R.string.already_answered);   			
        } else {	                   
	        // Create an ArrayAdapter using the answers		
	    	ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
			        R.array.Q1_answers, android.R.layout.simple_spinner_item);
	     	// Specify the layout to use when the list of choices appears
	     	adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	     	// Apply the adapter to the spinner
	     	mSpQ1Answers.setAdapter(adapter1);    
	     	mSpQ1Answers.setOnItemSelectedListener(new OnItemSelectedListener() {		     		
	            @Override
	            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {	
	            	// get the answer only upon user selection and ignore the selection during initialisation 
	            	q1Answer = parent.getItemAtPosition(pos).toString();  
		            //Log.d(TAG,"Answer selected: " + q1Answer);	            
	            }
	            
	            @Override
	            public void onNothingSelected(AdapterView<?> parent) {
	  				// default to the first item in the list
	            	q1Answer = parent.getItemAtPosition(0).toString();
	            	//Log.d(TAG,"No answer selected ");
	   			}			
	        });			     	
	        
	        // Create an ArrayAdapter using the answers
	    	ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
			        R.array.Q2_answers, android.R.layout.simple_spinner_item);
	     	// Specify the layout to use when the list of choices appears
	     	adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	     	// Apply the adapter to the spinner
	     	mSpQ2Answers.setAdapter(adapter2);    
	     	mSpQ2Answers.setOnItemSelectedListener(new OnItemSelectedListener() {		     		
	            @Override
	            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {	
	            	// get the answer only upon user selection and ignore the selection during initialisation 
	            	q2Answer = parent.getItemAtPosition(pos).toString();  
		            //Log.d(TAG,"Answer selected: " + q1Answer);	            
	            }
	            
	            @Override
	            public void onNothingSelected(AdapterView<?> parent) {
	  				// default to the first item in the list
	            	q2Answer = parent.getItemAtPosition(0).toString();
	            	//Log.d(TAG,"No answer selected ");
	   			}			
	        });		
        }     	
     	
       	mBtnSubmit.setOnClickListener(new View.OnClickListener() {
       		@Override
       		public void onClick(View v) {
       			String text = "";
       			// user didn't provide both answers
       			if (q1Answer.equals(getResources().getText(R.string.select_answer).toString()) ||
       					q2Answer.equals(getResources().getText(R.string.select_answer).toString())) {
       				text = getResources().getText(R.string.prompt_all_answer).toString();  
       				mPrizeQAnswered = false;
        		} else {
        			// user answers both questions correctly
        			if (q1Answer.equals(getResources().getText(R.string.q1_answer).toString()) &&
           					q2Answer.equals(getResources().getText(R.string.q2_answer).toString())) {
        				text = getResources().getText(R.string.both_correct).toString();
        			// user answers Q1 correctly
        			} else if (q1Answer.equals(getResources().getText(R.string.q1_answer).toString())) {
        				text = getResources().getText(R.string.q1_correct).toString();
        			// user answers Q2 correctly
        			} else if (q2Answer.equals(getResources().getText(R.string.q2_answer).toString())) {
        				text = getResources().getText(R.string.q2_correct).toString();
        			// user answers both questions wrongly
        			} else {
        				text = getResources().getText(R.string.both_wrong).toString();
        			}     
        			
        			// Disable the components to prevent the user answering the questions twice
        			mSpQ1Answers.setEnabled(false);
           			mSpQ2Answers.setEnabled(false);
           			mPrizeQAnswered = true;   
           			
           			// Log the user answers           			
           			mEALogManager.writeToLogFile("Extra Prize - Q1 Answer: " + q1Answer + " Q2 Answer: " + q2Answer + 
           					"\n Feedback: " + text + "\n");  
        		} 	       			
       			mTxtDialogError.setText(text); 
       		}
       	});	 
       	
       	mBtnClose.setOnClickListener(new View.OnClickListener() {
       		@Override
       		public void onClick(View v) {
       			mDialog.dismiss();
       		}
       	});	   
    	mDialog.show();	               
	}*/
	
	/**
	 * Display the clue instructions dialog
	 */
	private void displayInstructionsDialog() {			
    	
		mClueDialog = new Dialog(this); 
    	
    	//tell the Dialog to use the dialog_instructions.xml as it's layout description
       	mClueDialog.setContentView(R.layout.dialog_instructions);
        mClueDialog.setTitle(R.string.instructions);
  
        mTxtClueInstructions = (TextView) mClueDialog.findViewById(R.id.txtInstructions);
       	mBtnCloseClue = (Button) mClueDialog.findViewById(R.id.btnClose);	
       	mTxtClueInstructions.setText(mClue);
       	
       	mBtnCloseClue.setOnClickListener(new View.OnClickListener() {
       		@Override
       		public void onClick(View v) {
       			mClueDialog.dismiss();
       		}
       	});	   
    	mClueDialog.show();	               
	}
	
	/**
	 * Display clue solution dialog
	 */
	private void displaySolutionDialog() {			
    	
		mSolutionDialog = new Dialog(this); 
    	
    	//tell the Dialog to use the dialog_instructions.xml as it's layout description
       	mSolutionDialog.setContentView(R.layout.dialog_solution);
        mSolutionDialog.setTitle(R.string.solution);
       
        mTxtSolution = (TextView) mSolutionDialog.findViewById(R.id.txtSolution);
       	mBtnCloseSolution = (Button) mSolutionDialog.findViewById(R.id.btnClose);	
       	mTxtSolution.setText(mSolution);
       	
       	mBtnCloseSolution.setOnClickListener(new View.OnClickListener() {
       		@Override
       		public void onClick(View v) {
       			mSolutionDialog.dismiss();
       		}
       	});	   
    	mSolutionDialog.show();	               
	}
	
	/**
	 * Displays the different clues/tasks of the treasure hunt
	 */
	private void displayClue(){		
		// reset variables 
		mClue = "";
		mSolution = "";
		mGotThere = false;
		
		// hide the solution link
		mTxtSolutionLink.setVisibility(View.INVISIBLE);
		
		//Toast.makeText(this, "mCurrentStep " + mCurrentStep, Toast.LENGTH_SHORT).show();
		//Log.d(TAG,"displayClue " + mRoute.getSteps().size());
		if (mCurrentStep < mRoute.getSteps().size()) {
	    	mStep = mRoute.getSteps().get(mCurrentStep);
	    	Log.d(TAG,"mCurrentStep " + mCurrentStep + " NIPoints " + mValence.getNIPoints());
	    	//Toast.makeText(this, "mStep " + mStep.toString(), Toast.LENGTH_SHORT).show();
	    	// retrieving the task
	    	mClue = mStep.getTask(); 
	    	// the last step
	    	if (mCurrentStep == (mRoute.getSteps().size() - 1)) {
	    		if (mValence.getUnlocked() >= HWUGeneral.TREASURE_THRESHOLD) {
	    			mClue += getResources().getText(R.string.draw).toString();
	    		}
                this.showFinishDialog();
	    		mClue += ". " + getResources().getText(R.string.finish).toString();
	    	}
	    	mClue += "\n";
	    	mSolution = mStep.getSolution() + "\n";
	    	Log.d(TAG,"mClue " + mClue); 
	    	
	    	// create geofences if points exists
	    	createGeofences();
	    	
	    	// get the questions 
	    	getQuestions();
	    	
	    	// get the related treasure
	    	getTreasure();
	    	
	    	// note the start time of clue
	    	mOnTheMove = false;
	    	mClueStartTime = SystemClock.elapsedRealtime();
	    	mClueStarted = true;
	    	
			// Emys speaking
			emysSpeak(Emotion.NEUTRAL, mClue);
			
			// Log the step
			mEALogManager.writeToLogFile("Clue: " + mStep.getName() + "\n" + mClue);
			
			// intervention is not needed for the first and the last step
			if (mCurrentStep < (mRoute.getSteps().size()-1)) {
				mValence.setNIPoints(mCurrentStep*HWUGeneral.INTERVENTION_NO);
			   	mInterventions = 0;
			}
						
			// increase currentStep for next iteration
			mCurrentStep++;			
			
			// switch to question mode where an answer is required after displaying the clue
			if (mQuestions.size() > 0) {
				mTreasureHuntMode = HWUGeneral.QUESTION;
	    	}	
		}
	}  
	
	/**
     * Create geofences for the points in this step
     */
    private void createGeofences() {
    	// clear existing geofence lists
    	clearGeofenceLists();
    	//Toast.makeText(this, "mStep" + mStep.toString(), Toast.LENGTH_SHORT).show();
    	// check if gps points exist
    	mPoints = mStep.getPoints();
    	//Toast.makeText(this, "mCurrentPoint in createGeofences " + mCurrentPoint + mPoints.size(), Toast.LENGTH_SHORT).show();
    	Log.d(TAG,"mCurrentPoint : " + mCurrentPoint);
    	Log.d(TAG, "mPoints : " + mPoints.toString());
    	if (mPoints.size() > 0) {		    		
    		// user is not at destination
	    	if(mCurrentPoint < mPoints.size()) {
	    		mAtDestination = false;
	    		// add all the rest of points for geofence creation
	    		for (int i = mCurrentPoint; i < mPoints.size(); i++){
	    			mGeofencePoints.add(mPoints.get(i));
	    		}
	    		mPoint = mPoints.get(mCurrentPoint++);  
	    		// set the new destination point
	    		setNextDestination();
	        	Log.d(TAG, "mPoint : " + mPoint.toString());
	        	//Toast.makeText(this,  "mPoint to create : " + mPoint.toString(), Toast.LENGTH_SHORT).show();
	    		Log.i(TAG,"adding geofences : " + mGeofencePoints.toString());
	    		//Toast.makeText(getApplicationContext(), "adding geofence: " + mGeofencePoints.toString(), Toast.LENGTH_SHORT).show();
	    		// populate the geofence list using the points info
	            mGeofenceHandler.populateGeofenceList(mGeofencePoints); 
	            mGeofenceHandler.addGeofence();
	    	}
    	}
    }
    
    /**
     * Delete all geofences except the final clue geofence
     */
    private void getClueGeofence() {      	
    	List<String> geofenceIdsToRemove = new ArrayList<String>();
    	
    	// reduce the current point counter which was increased in geofence receiver
    	if (mCurrentPoint > 0) {
    		mCurrentPoint--;
    	}
    	
    	// add all geofences to remove to a list
		for (; mCurrentPoint < mPoints.size()-1; mCurrentPoint++){
	    	geofenceIdsToRemove.add(mPoints.get(mCurrentPoint).getName());
	    	//Toast.makeText(this, "id to remove" + mPoints.get(mCurrentPoint).getName(), Toast.LENGTH_SHORT).show();
	    }
	
		// remove the geofences in the list
		if (geofenceIdsToRemove.size() > 0) {
			mGeofenceHandler.removesGeofence(geofenceIdsToRemove);
			//Toast.makeText(this, "removing ", Toast.LENGTH_SHORT).show();
			Log.i(TAG, "Removing " + geofenceIdsToRemove.toString());
		}
    	
    	// get the clue destination
		//Toast.makeText(this, "mCurrentPoint " + mCurrentPoint, Toast.LENGTH_SHORT).show();
		mPoint = mPoints.get(mCurrentPoint++);
		//Toast.makeText(this, "mPoint" + mPoint.getName(), Toast.LENGTH_SHORT).show();
		//Toast.makeText(this, "mCurrentPoint " + mCurrentPoint + " " + mPoint.getName(), Toast.LENGTH_SHORT).show();
		mAtDestination = false;
		setNextDestination();
			
    	// check if gps points exist
    	/*mPoints = mStep.getPoints();    	
    	if (mPoints.size() > 0) {	
    		mCurrentPoint = mPoints.size()-1;
    		Toast.makeText(this, "mCurrentPoint " + mCurrentPoint, Toast.LENGTH_SHORT).show();
        	Log.d(TAG,"mCurrentPoint : " + mCurrentPoint);
        	Log.d(TAG, "mPoints : " + mPoints.toString());
        	
    		// user is not at destination
	    	if(mCurrentPoint < mPoints.size()) {
	    		mAtDestination = false;
	    		// add all the rest of points for geofence creation
	    		for (int i = mCurrentPoint; i < mPoints.size(); i++){
	    			mGeofencePoints.add(mPoints.get(i));
	    		}
	    		mPoint = mPoints.get(mCurrentPoint++);  
	    		// set the new destination point
	    		setNextDestination();
	        	Log.d(TAG, "mPoint : " + mPoint.toString());
	    		Log.i(TAG,"adding geofences : " + mGeofencePoints.toString());
	    		//Toast.makeText(getApplicationContext(), "adding geofence: " + mGeofencePoints.toString(), Toast.LENGTH_SHORT).show();
	    		// populate the geofence list using the points info
	            mGeofenceHandler.populateGeofenceList(mGeofencePoints); 
	            mGeofenceHandler.addGeofence();
	    	}
    	}*/
    }
    
    
	/**
	 * Get questions for step
	 */
	private void getQuestions() {
		// check if questions exist
    	mQuestions = mStep.getQuestions();
	}

	@Override
	public void imageDownloaded(Drawable image) {
		mTreasure.setImageDrawable(image);
	}

	public void imageNotDownloaded() {
		Drawable placeholderTreasure = getResources().getDrawable(R.drawable.treasure_placeholder);
		mTreasure.setImageDrawable(placeholderTreasure);
	}
	
	/**
	 * Get the related treasure
	 */
	private void getTreasure() {
		// check if treasure exists
    	String treasure = mStep.getTreasure();
		if (treasure.length() > 0) {
			int treasureStepId = (mStep.getStepNo() - 1);
			if (treasureStepId < 0) {
				treasureStepId = 0;
			}
			String treasureName = "treasure" + treasureStepId;
			int treasureId = getResources().getIdentifier(treasureName, "id", getPackageName());
			mTreasure = (ImageView) findViewById(treasureId);

			imageDownloader = new DownloadImage();
			imageDownloader.delegate = this;
			imageDownloader.execute(treasure);
		}
	}
	
	/**
	 * Displays the questions related to each clue
	 */
	private void displayQuestion(){		
		String strQuestion = "";
		mAnswer = null;
		
		if (mCurrentQuestion < mQuestions.size()) {
			if (mCurrentQuestion == 0) {
				// question start time
				mAnsweredQuestion = false;
				mQuestionStartTime = SystemClock.elapsedRealtime();
				mQuestionStarted = true;
			}
			
	    	final Question question = mQuestions.get(mCurrentQuestion); 
			strQuestion = question.getQuestion() + "\n";
			
			// if an answer if required
			if (question.getAnswers().size() > 0) {				
				// display answers
				displayAnswers(question);
				
		    	// switch to answer mode after displaying the question
				mTreasureHuntMode = HWUGeneral.ANSWER;		
			} else {
				// switch to clue mode if the question does not require an answer on screen
				//mTreasureHuntMode = CLUE;
				mTreasureHuntMode = HWUGeneral.CHANGE;
			}	     	
			// Emys speaking
			emysSpeak(Emotion.NEUTRAL, strQuestion);
			
			// Log the question
			mEALogManager.writeToLogFile("Question: " + strQuestion);
		} 		
	}
	
	/**
	 * Display the available answers
	 */
	private void displayAnswers(Question question) {
		ArrayList<String> answers = new ArrayList<String>();
		if (question.getIsImage()) {
			int size = question.getAnswers().size();
			mImageIcons = new Integer[size];
			for (int i=0; i<size; i++) {	
				// Create an answer list with image icons	
				mImageIcons[i] = getImageResourceId(question.getAnswers().get(i));
			}
			// Create an ArrayAdapter using the answers
			ImageArrayAdapter adapter = new ImageArrayAdapter(this, mImageIcons);	
			// Specify the layout to use when the list of choices appears
	     	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	     	// Apply the adapter to the spinner
	     	spAnswers.setAdapter(adapter);   
		} else {				
			// Create an answer list with the instruction as the first element				
			answers.add(getResources().getText(R.string.select_answer).toString());
			answers.addAll(question.getAnswers());
			// Create an ArrayAdapter using the answers
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
	     		android.R.layout.simple_spinner_item, answers);
			// Specify the layout to use when the list of choices appears
	     	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	     	// Apply the adapter to the spinner
	     	spAnswers.setAdapter(adapter);   
		}
      
     	spAnswers.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				// get the answer only upon user selection and ignore the selection during initialisation
				mAnswer = parent.getItemAtPosition(pos).toString();
				//Log.d(TAG,"Answer selected: " + mAnswer);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// default to the first item in the list
				mAnswer = parent.getItemAtPosition(0).toString();
				//Log.d(TAG,"No answer selected ");
			}
		});
     	spAnswers.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Displays the feedback to an answer
	 */
	private void displayFeedback() {
		FeedbackItem fbItem = new FeedbackItem();
		spAnswers.setVisibility(View.INVISIBLE);	
		Random rand = new Random();
		String correctAnswer = mQuestions.get(mCurrentQuestion).getCorrectAnswer();
		int correctImageId = -1;
		String strFeedback = "";
		
		// increase total questions answered
		mValence.increaseTotalQ();
				
		// get the image id for the correct answer
		if (mQuestions.get(mCurrentQuestion).getIsImage()) {
			correctImageId = getImageResourceId(correctAnswer);
			correctAnswer = String.valueOf(correctImageId);
		} 
			
		// correct answer
		if (mAnswer.equals(correctAnswer)) {
			int i = rand.nextInt(mFeedback.getPositiveFeedback().getFeedbackItems().size());
			fbItem = mFeedback.getPositiveFeedback().getFeedbackItem(i);
			strFeedback = fbItem.getText() + getResources().getText(R.string.thats_correct).toString() + "\n";
			mCorrectAnswers++;
			mCorrectAnswersAll++;
			mValence.increaseCorrect();
			if (mTreasure != null) {
				mTreasure.setVisibility(View.VISIBLE);
			}
		} else {
			int i = rand.nextInt(mFeedback.getNegativeFeedback().getFeedbackItems().size());
			fbItem = mFeedback.getNegativeFeedback().getFeedbackItem(i);
			strFeedback = fbItem.getText() + ". " + 
					getResources().getText(R.string.correct_answer).toString() + " ";
			
			// display the correct symbol
			if (mQuestions.get(mCurrentQuestion).getIsImage()) {
				mSymbol.setVisibility(View.VISIBLE);
				mSymbol.setBackgroundResource(correctImageId);
			} else {
				strFeedback += correctAnswer + ".\n";
			}
		}	
		
		// check correct answers
		checkCorrectAnswers();
		
		// Emys speaking
		emysSpeak(Emotion.getEmotion(fbItem.getGesture()), strFeedback);	
		
		if (mQuestions.get(mCurrentQuestion).getIsImage()) {
			correctAnswer = mQuestions.get(mCurrentQuestion).getCorrectAnswer();
		}
		// Log the feedback
		mEALogManager.writeToLogFile("Selected answer: " + mAnswer + "\n Feedback: " + strFeedback + "\n\n");
				
		// increase question count
		mCurrentQuestion++; 
		
		// reset question count if all questions for this clue has been presented
		if (mCurrentQuestion == mQuestions.size()) {
			mCurrentQuestion = 0;
			mCorrectAnswers = 0;
			//mTreasureHuntMode = CHANGE;
			mTreasureHuntMode = HWUGeneral.CLUE;
			spAnswers.setVisibility(View.INVISIBLE);
			mValence.increaseTotalTreasures();
		} else {
			mTreasureHuntMode = HWUGeneral.QUESTION;
		}
	}
	
	/** 
	 * Check if user answers all questions correctly, if yes, play an emblem
	 */
	private void checkCorrectAnswers() {
		if (mCorrectAnswers == mQuestions.size()) {
			// Get a random sound emblem
			Random ran = new Random();
			int x = ran.nextInt(HWUGeneral.EMBLEMS_NO) + 1;
			int emblemID = getRawResourceId("positive" + String.valueOf(x));			
			playEmblem(emblemID);
			// Treasure is unlock
			if (mTreasure != null) {
				mTreasure.setVisibility(View.VISIBLE);
			}
			mValence.increaseUnlocked();
		}	
	}
	
	/** 
	 * Displays the instruction to take turn
	 */
	private void displayChangeInstruction() {
		// take turn instruction
		String text = getResources().getText(R.string.take_turn).toString();
		// Emys speaking
		emysSpeak(Emotion.NEUTRAL, text);		
		// switch to clue mode
		mTreasureHuntMode = HWUGeneral.CLUE;	
	}
	
	/** 
	 * Emys speaking
	 */
	private void emysSpeak(Emotion emotion, String utterance) {
		// update subtitles
		mTxtSubtitles.setText(utterance);
		// set Emys emotion
		setExpression(emotion);
		// speak text
		speakText(utterance);
		mTTSText = utterance;
		// disable the proceed and play again buttons while Emys is speaking
		disableButtons();
	}
	
	/**
	 * Interrupt user when one of the intervention scenario occurs
	 */
	private void emysIntervene(Emotion emotion, String utterance) {
		// toast utterance
		Toast.makeText(getApplicationContext(), utterance, Toast.LENGTH_LONG).show();
		// set Emys emotion
		setExpression(emotion);
		// speak text
		speakText(utterance);
		mTTSText = utterance;
		// disable the proceed and play again buttons while Emys is speaking
		disableButtons();
		// retoast to make sure that the user have enough time to read the utterance
		Toast.makeText(getApplicationContext(), utterance, Toast.LENGTH_LONG).show();
	}
	
	/**
	 * disable the proceed and play again buttons 
	 */
	private void disableButtons() {
		mBtnProceed.setEnabled(false);
		mBtnPlayAgain.setEnabled(false);
	}
	
	/**
	 * enable the proceed and play again buttons
	 */
	private void enableButtons() {
		mBtnGetBaseline.setVisibility(View.INVISIBLE);
		mBtnProceed.setVisibility(View.VISIBLE);
		mBtnPlayAgain.setVisibility(View.VISIBLE);
		mBtnProceed.setEnabled(true);
		mBtnPlayAgain.setEnabled(true);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.eatreasure_hunt, menu);
		return true;
	}

	@Override
	protected LinearLayout getGlSurfaceViewContainerLayout() {		
		return (LinearLayout) this.findViewById(R.id.glLayoutHolder);
	}

	@Override
	protected void onEmysLoaded() {
		mDi.unBlankScreen();
		mBtnProceed.setText(R.string.next);

		if (mRunningMode == HWUGeneral.TreasureHuntRunningMode.DEMONSTRATION) {
			Toast.makeText(getApplicationContext(), getResources().getText(R.string.baseline_recorded).toString(), Toast.LENGTH_SHORT).show();
			// Emys speaking
			emysSpeak(Emotion.NEUTRAL, getResources().getText(R.string.baseline_recorded).toString() + " " + getResources().getText(R.string.start_instruction).toString());
			//Toast.makeText(getApplicationContext(), "Baseline : " + mArousal.getBaseline(), Toast.LENGTH_SHORT).show();
			mTreasureHuntMode = HWUGeneral.CLUE;
			enableButtons();
		} else if (!mBaseline) {
			mBtnProceed.setVisibility(View.INVISIBLE);
			mBtnPlayAgain.setVisibility(View.INVISIBLE);
		} else {
			enableButtons();
		}
	}

	@Override
	public void confirmDialog(String infoText) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dismissConfirmDialog() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void askMultiChoice(Integer numChoices, String[] options) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dismissMultiDialog() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void migrateDataIn(HashMap<String, String> migData) {
		//make sure the interpreter's not running.
		mDialogSystem.interruptDialogEvent();
		mDi.unBlankScreen();
		playMigrateInSound();
		putMigrationData(migData);
	}

	@Override
	public void navigateFromTo(String from, String to, String callback) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getFreeTextDialog() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageView view = (ImageView) v;
    	switch (event.getAction() & MotionEvent.ACTION_MASK) {
    		// user touches the screen
    		case MotionEvent.ACTION_DOWN:

    			mSavedMatrix.set(mMatrix);
    			//mSavedScaleMatrix.set(mScaleMatrix);
    			//Log.d(TAG,"mSavedMatrix=" + mSavedMatrix.toString());
    			mStartPoint.set(event.getX(), event.getY());
    			mode = DRAG;
    			break;
    			
    		// user touches the screen with two fingers 
    		case MotionEvent.ACTION_POINTER_DOWN:
	
    			mOldDist = spacing(event);
	
    			if (mOldDist > 10f) {
    				mSavedMatrix.set(mMatrix);
    				//mSavedScaleMatrix.set(mScaleMatrix);
    				midPoint(mMidPoint, event);
    				mode = ZOOM;
    			}
    			break;
	
    		// user releases the touch
    		case MotionEvent.ACTION_UP:
    			mode = NONE;
    			break;
	
    		case MotionEvent.ACTION_POINTER_UP:
    			mode = NONE;	
    			break;
	
    		case MotionEvent.ACTION_MOVE:    			
    			if (mode == DRAG) {       				    				
    				mMatrix.set(mSavedMatrix);
    				// calculate the distance for translation
    				float xDist = event.getX() - mStartPoint.x;
    				float yDist = event.getY() - mStartPoint.y;
    				
    				// retrieving the matrix scale and translations to check that the final values are within boundaries
    				float[] f = new float[9];    				
    				mMatrix.getValues(f);
    				float scale = f[Matrix.MSCALE_X];
    				float transX = f[Matrix.MTRANS_X];
    				float transY = f[Matrix.MTRANS_Y]; 
    				
    				// limit the translation boundaries taking into consideration the scale of the map
    				float mMaxX = mTreasureMap.getWidth()/2;
    				float mMinX = - (mTreasureMap.getWidth() * 1.5f) * scale;
    				float mMaxY = mTreasureMap.getHeight()/2;
    				float mMinY = - (mTreasureMap.getHeight() * 1.75f) * scale ;
    				//Log.d(TAG,"Max Min " + mMaxX + " " + mMinX + " " +  mMaxY + " " + mMinY + " mScale " + scale);    
    				//Log.d(TAG,"xDist yDist before" + xDist + " " + yDist);
    				
    				// calculate the new position
    				float totalX = transX + xDist;
    				float totalY = transY + yDist;
					
    				// setting the boundaries
    				if (totalX > mMaxX) {
						xDist = mMaxX-totalX;    					
					}
					if (totalX < mMinX) {
						xDist = mMinX-totalX;
					}
					if (totalY > mMaxY) {
						yDist = mMaxY-totalY;
					}
					if (totalY < mMinY) {
						yDist = mMinY-totalY;
					}
					
					// apply the translation
					mMatrix.postTranslate(xDist, yDist); 
					//Log.d(TAG,"xDist yDist after" + xDist + " " + yDist + " " + transX + " " + transY);
    				//Log.d(TAG,"matrix=" + mMatrix.toString());					
    			} else if (mode == ZOOM) {
    				float[] f = new float[9];
    				float newDist = spacing(event);
    				mScale = 1f;
    			
    				if (newDist > 10f) {
    					mMatrix.set(mSavedMatrix);
    					//mScaleMatrix.set(mSavedScaleMatrix);
    					mScale = newDist / mOldDist;
    					//Log.d(TAG,"newDist " + newDist + " oldDist " + mOldDist);
    					mMatrix.postScale(mScale, mScale, mMidPoint.x, mMidPoint.y);
    					//mScaleMatrix.postScale(mScale, 1.0f, 0.0f, 0.0f);    					
    				}
    				
    				// retrieving the matrix scales
    				mMatrix.getValues(f);
    				float scaleX = f[Matrix.MSCALE_X];
    				//float scaleY = f[Matrix.MSCALE_Y];
    				
    				// the minimum and maximum zoom levels
    				if(scaleX <= MIN_ZOOM) {
    					mScale = MIN_ZOOM/scaleX;
    				}    	
    				else if(scaleX >= MAX_ZOOM) {
    					mScale = MAX_ZOOM/scaleX;
       				} 
    				mMatrix.postScale(mScale, mScale, mMidPoint.x, mMidPoint.y);
    				//mScaleMatrix.postScale(mScale, 1.0f, 0.0f, 0.0f); 
    			}    			
    			break;	
	     }
    	 view.setImageMatrix(mMatrix);
    	 //mMapScale.setImageMatrix(mScaleMatrix);
	     return true;
	}
	
	/**
	 * Determine the space between the first two fingers
	 */
	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
	    float y = event.getY(0) - event.getY(1);
	    return (float)Math.sqrt(x * x + y * y);
	}

	/**
	 * Calculate the mid point of the first two fingers
	 */
	private void midPoint(PointF point, MotionEvent event) {
	    float x = event.getX(0) + event.getX(1);
	   	float y = event.getY(0) + event.getY(1);
	   	point.set(x / 2, y / 2);
	}
	
    /**
     * Get the resource ID for a symbol
     * @param symbolName
     * @return resource ID of the image
     */
    private int getResourceId(String symbolName) {
    	Resources resources = getApplicationContext().getResources();
		
		int resourceId = resources.getIdentifier(symbolName, 
				"id", getApplicationContext().getPackageName());
		return resourceId;
    }
    
    /**
     * Get the image resource ID for a symbol
     * @param symbolName
     * @return resource ID of the image
     */
    private int getImageResourceId(String symbolName) {
    	Resources resources = getApplicationContext().getResources();
		
		int resourceId = resources.getIdentifier(symbolName, 
				"drawable", getApplicationContext().getPackageName());
		return resourceId;
    }
    
    /**
     * Get the resource ID for a string
     * @param stringName
     * @return resource ID of the string
     */
    private int getStringResourceId(String stringName) {
    	Resources resources = getApplicationContext().getResources();
		
		int resourceId = resources.getIdentifier(stringName, 
				"string", getApplicationContext().getPackageName());
		return resourceId;
    }
    
    /**
     * Get the resource ID for the sound emblem 
     * @param rawName
     * @return resource ID of the sound emblem
     */
    private int getRawResourceId(String rawName) {
    	Resources resources = getApplicationContext().getResources();
		
		int resourceId = resources.getIdentifier(rawName, 
				"raw", getApplicationContext().getPackageName());
		return resourceId;
    }
    
    /**
     * Check if this is an emphatic agent 
     * @return true if emphatic agent
     */
    private boolean isAgentWithEmpathy() {
    	if (mSelectedCondition.equals(HWUGeneral.EMPATHIC)) 
    		return true;
		return false;
    }
    
    /**
     * Check if user is frustrated
     * @return true if user is frustrated
     */
    private boolean isUserFrustrated() {
    	//if (mArousal.getArousal().equals(Arousal.HIGH) && ) 
    		return true;
		//return false;
    }
    
    /** 
     * Getting all elements of a string arraylist 
     * @param elements
     * @return a string of the elements
     */
    private String getAllElements(ArrayList<String> elements) {
    	String strElements = "";
    	// get all the elements in the ArrayList except the last two	                    	
    	for (int i = 0; i < elements.size() - 2; i++) {
    		strElements += elements.get(i) + ", ";
    	}	   
    	// get the last two elements in the ArrayList
    	strElements += elements.get(elements.size()-2) + " and " + elements.get(elements.size()-1);
    	return strElements;
    }
    
    /**
     * Play the sound emblem
     * @param emblemID
     */
    private void playEmblem(int emblemID) {
    	mMediaPlayer = MediaPlayer.create(this, emblemID);
		mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {	
			@Override
			public void onCompletion(MediaPlayer mp) {
				mDi.stopWaitingAndNotify();
			}
		});
		mMediaPlayer.start();
    }

	@Override
	public void putMigrationData(HashMap<String, String> data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playMigrateOutSound() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playMigrateInSound() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Start the location service
	 */
	private void startLocationService() {
		// start the location service - only once as it will be running continuously in the 
        // background until the app is destroyed
	    Intent locationService = new Intent( this, LocationService.class );
	    locationService.putExtra(HWUGeneral.LOG_FILE, mLogFileName);
	    startService(locationService);
	    // register broadcast receiver for location
	    registerReceiver(locationReceiver, new IntentFilter(LocationService.BROADCAST_LOCATION));
	}

	/**
	 * Stop the location service
	 */
	private void stopLocationService(){
		// stop the location service
	    Intent locationService = new Intent( this, LocationService.class );
	    stopService(locationService);
	    // unregister broadcast receiver
	    unregisterReceiver(locationReceiver);
	}
	
	/**
	 * Start the geofence service
	 */
	private void startGeofenceService() {
		// start the geofence service
	    //Intent geofenceService = new Intent( this, GeofenceService.class );
	    //startService(geofenceService);
	    // register broadcast receiver for location
	    registerReceiver(geofenceReceiver, new IntentFilter(GeofenceService.BROADCAST_GEOFENCE));
	}

	/**
	 * Stop the geofence service
	 */
	private void stopGeofenceService(){
		// stop the geofence service
	    Intent geofenceService = new Intent( this, GeofenceService.class );
	    stopService(geofenceService);
	    // mGeofenceHandler.removesGeofence();
	    // unregister broadcast receiver
	    unregisterReceiver(geofenceReceiver);
	}
	
	/**
	 * Start the bearing service
	 */
	private void startBearingService(){
		// start the bearing service
	    Intent bearingService = new Intent( this, BearingService.class );
	    startService(bearingService);
	    
	    // register broadcast receivers for bearing and declination
	    registerReceiver(azimuthReceiver, new IntentFilter(BearingService.BROADCAST_AZIMUTH));	
	}
	
	
	/**
	 * Stop the bearing service
	 */
	private void stopBearingService() {
		 // stop the bearing service
	    Intent bearingService = new Intent( this, BearingService.class );
	    stopService(bearingService);
	    // unregister broadcast receiver
	    unregisterReceiver(azimuthReceiver);
	}
	
	/** 
	 * Broadcast receiver for the azimuth value 
	 */
	private BroadcastReceiver azimuthReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	// Get the Azimuth
        	mAzimuth = intent.getFloatExtra(BearingService.AZIMUTH, mCurrentDegree);
    		updateCompass();
        }
    };  
    
    /** 
	 * Broadcast receiver for user location 
	 */
    private BroadcastReceiver locationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {   
        	// set the current location
			double latitude = intent.getDoubleExtra(LocationService.LATITUDE, 0.0);
			double longitude = intent.getDoubleExtra(LocationService.LONGITUDE, 0.0);
			float declination = intent.getFloatExtra(LocationService.DECLINATION, 0.0f);

			handleLocationChanged(latitude, longitude, declination);
        }
    };

	private void handleLocationChanged(double latitude, double longitude, float declination) {
		mUserTracking.setCurrentLocation(latitude, longitude);
		mDeclination = declination;
		updateCompass();

		// Check the bearing and movement
		if (mUserTracking.getNextDestination() != null) {
			// user is on the move
			if (!mOnTheMove) {
				mOnTheMove = true;
				//mClueStarted = false;
			}

			// ********** debugging
			// TODO to be deleted
	            /*// Calculate the bearing to destination
	            double destinationBearing = mUserTracking.getDestinationBearing();
	            // Calculate user's bearing
	            double userBearing = mUserTracking.getUserBearing();
	            /*mTxtBearing.setText("Bearing: " + destinationBearing);
	            mTxtUserBearing.setText("User: " + userBearing);
	            mTxtHeartRate.setText("Heart Rate: " + mArousal.getHeartRate());*/

			// Calculate the user distance from the destination
			if (mAtDestination && !mGotThere) {
				if (mUserTracking.getDistanceToDestination() > PASSED_DESTINATION_THRESHOLD) {
					// *********** User has passed the destination ***********
					intervene(HWUGeneral.PASSED_DESTINATION, HWUGeneral.MOVING);
				}
			} else if (mTreasureHuntMode != HWUGeneral.SOLUTION) {
				// get the difference between destination bearing and user bearing
				double bearingDiff =  mUserTracking.getDestinationBearing() - mUserTracking.getUserBearing();
				//Log.d(TAG,"bearing diff: " + bearingDiff + " mWrongDirection " + mWrongDirection);

				// intervene when the user is heading in the wrong direction
				if (bearingDiff < -BEARING_THRESHOLD || bearingDiff > BEARING_THRESHOLD) {
					if (!mWrongDirection) {
						mWrongDirection = true;
						mWrongLocStart.set(mUserTracking.getCurrentLocation());
						mWrongDirStartTime = SystemClock.elapsedRealtime();
						mDistanceToDestination = mUserTracking.getDistance(mUserTracking.getCurrentLocation(), mUserTracking.getNextDestination());
						//Log.d(TAG, "wrong direction " + mWrongDirection);
						//Toast.makeText(this, "Wrong direction", Toast.LENGTH_LONG).show();
					} else {
						//Log.d(TAG,"wrong latitude: " + mWrongLocStart.getLatitude() + " longitude: " + mWrongLocStart.getLongitude());
						//Log.d(TAG,"current latitude: " + mUserTracking.getCurrentLocation().getLatitude() + " longitude: " + mUserTracking.getCurrentLocation().getLongitude());
						// calculate the distance from the user stray point
						float wrongDistance = mUserTracking.getDistance(mWrongLocStart, mUserTracking.getCurrentLocation());
						long wrongTime = SystemClock.elapsedRealtime() - mWrongDirStartTime;
						float distanceToDestination = mUserTracking.getDistance(mUserTracking.getCurrentLocation(), mUserTracking.getNextDestination());

						if ((wrongTime > WRONG_DIR_TIME_THRESHOLD) && (wrongDistance > WRONG_DIRECTION_THRESHOLD) && (distanceToDestination > mDistanceToDestination)) {
							// ************* User is going in the wrong direction ***********
							//Toast.makeText(getApplicationContext(), "Wrong direction", Toast.LENGTH_LONG).show();
							intervene(HWUGeneral.WRONG_DIRECTION, HWUGeneral.MOVING);
							resetWrongDirVars();
						}
					}
				} else { // reset the mWrongDirection to false
					resetWrongDirVars();
				}
			}
		}
	}
    
    /**
     * Resetting the wrong direction Variables
     */
    private void resetWrongDirVars() {
    	mWrongDirection = false;
    	mDistanceToDestination = INIT_DISTANCE;
    }
    
    /**
     * Set the next destination point 
     */
    private void setNextDestination() {
    	if(mPoint != null) {
    		 //Log.d(TAG,"next destination: " + mPoint.getName());
    		 mUserTracking.setNextDestination(mPoint.getLatitude(), mPoint.getLongitude());
    		 //mTxtNextDestination.setText("Destination: " + mPoint.getName());    		 
    		 // Set the arousal range for the current point
    		 mArousal.setMRange(mPoint.getHr().getMRange());
    		 mArousal.setSRange(mPoint.getHr().getSRange());
    	}
    }
    
    /**
     * Updates the compass bearing
     */
    private void updateCompass() {
    	if (!Float.isNaN(this.mAzimuth)) {
            if(Float.isNaN(mDeclination)) {
                //Log.d(TAG, "Location is NULL bearing is not true north!");
                mBearing = mAzimuth;
            } else {
            	mBearing = mAzimuth + mDeclination;            	
            	//Log.d(TAG, "azimuth " + mAzimuth + " bearing " + mBearing);
            }
    	}
    	
    	RotateAnimation ra = new RotateAnimation(
        		mCurrentDegree, 
                -mBearing,
                Animation.RELATIVE_TO_SELF, 0.5f, 
                Animation.RELATIVE_TO_SELF,
                0.5f);
 
        ra.setDuration(250);
 
        ra.setFillAfter(true);
 
        mCompass.startAnimation(ra);
        mCurrentDegree = -mBearing;	
    }
    
    /**
     * Broadcast receiver for geofence transitions
     */
    private BroadcastReceiver geofenceReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {	    	
            String action = intent.getAction();
            //Toast.makeText(context, "Geofence update" + action, Toast.LENGTH_SHORT).show();
            
            String error = intent.getStringExtra(GeofenceService.GEOFENCE_ERROR);
            String transition = intent.getStringExtra(GeofenceService.GEOFENCE_TRANSITION);
            String invalid_transition = intent.getStringExtra(GeofenceService.INVALID_TRANSITION);            
            
            // Intent contains information about errors in adding or removing geofences
            if (!TextUtils.equals(error, null)) {	
            	Log.i(TAG, "Geofence Error : " + error);
            // Intent contains information about a geofence transition
            } else if (!TextUtils.equals(transition, null)) {                   	
            	Log.i(TAG, "Transition details: " + transition);
            	//Toast.makeText(context, transition, Toast.LENGTH_SHORT).show();
            	
            	// Removing the geofence 
            	String geofenceIds = intent.getStringExtra(GeofenceService.GEOFENCE_IDS);
            	if (!TextUtils.equals(geofenceIds, null)) {
	     			Log.i(TAG, "all the geofences entered " + geofenceIds);
	     			StringTokenizer geofences = new StringTokenizer(geofenceIds, ",");
	     			String geofenceId = "";
	     			List<String> geofenceIdsToRemove = new ArrayList<String>();
	     			if (mPoint != null) {
		     			while (geofences.hasMoreTokens()) {
		     				geofenceId = geofences.nextToken();
		     				//Log.d(TAG, "current geofenceId " + geofenceId + " mPoint " + mPoint.getName());
			     			//Toast.makeText(context, "Congratulations! You have reached " + geofenceId, Toast.LENGTH_SHORT).show();
			     			// check if the geofence is the next point
			     			if (TextUtils.equals(geofenceId, mPoint.getName())) {
			     				handlePointReached(geofenceId);
								geofenceIdsToRemove.add(geofenceId);
							}
		     			}		     			
		     			if (geofenceIdsToRemove.size() > 0) {
		     				mGeofenceHandler.removesGeofence(geofenceIdsToRemove);
		     				Log.i(TAG, "Removing " + geofenceIdsToRemove.toString());
		     			}
	     			}
            	}
            // The Intent contained an invalid action
            } else if (!TextUtils.equals(invalid_transition, null)) {
            	Log.i(TAG, invalid_transition);
    			//Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
            }
		}
    };

	private void handlePointReached(String pointName) {
		// user not at destination yet
		if (!mAtDestination && mCurrentPoint < mPoints.size()) {
			// get the next point
			mPoint = mPoints.get(mCurrentPoint++);
			setNextDestination();
			Log.i(TAG, "mPoint : " + mPoint.toString());
		} else {
			// change treasure hunt mode to Q & A if it was set to SOLUTION when user click for help
			mTreasureHuntMode = HWUGeneral.QUESTION;
			mAtDestination = true;
			mCurrentPoint = 0;
		}

		// pumpkin for dual tasks
		mMapTreasure.setImageDrawable(mTreasure.getDrawable());
		mMapTreasure.setVisibility(View.VISIBLE);
		mTreasureStartTime = SystemClock.elapsedRealtime();
		mTotalTreasures++;
		mTreasureVisible = true;
	}
}
