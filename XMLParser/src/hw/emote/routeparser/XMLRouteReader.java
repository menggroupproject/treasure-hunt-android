package hw.emote.routeparser;

import hw.emote.xmlparser.XMLReader;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Scanner;

import android.location.Location;

/**
 * @author Mei Yii Lim
 *
 * This class reads the route.xml files and return a list of Route objects.
 * 
 */

public class XMLRouteReader extends XMLReader{
	//private final static String STEP_FILE = "steps.xml"; 
	private ArrayList<Route> routes;
	private Document document;
	
	public XMLRouteReader(InputStream isRoute) throws JSONException, MalformedURLException{
		try {
			document = parse(isRoute);
			routes = getRoutes(document);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<Route> getRoutes(){
		return routes;
	}

	public String getXML() {
		return document.asXML();
	}
	
	private ArrayList<Route> getRoutes(Document scriptDoc) throws DocumentException, JSONException {
		ArrayList<Route> routes = new ArrayList<Route>();

		Element routesList = scriptDoc.getRootElement();
		Element root = (Element) routesList.elementIterator().next();
		Integer in = 0;
		for (Iterator i = root.elementIterator(); i.hasNext(); ) {
            Element element = (Element) i.next();
            if (element.getName().equals("routes")){
            	Route route = new Route();
                ArrayList<Step> steps = new ArrayList<Step>();
            	for (Iterator j = element.elementIterator(); j.hasNext();){            		
            		Element temp = (Element) j.next();
            		if (temp.getName().equals("name")){
            			route.setName(temp.getText());
            		} else if (temp.getName().equals("steps")){
            			for (Iterator k = temp.elementIterator(); k.hasNext();){
                    		Element e = (Element) k.next();
                    		Step step = getStepInfo(e);
                    		steps.add(step);
                    		//System.out.println(step.toString());
            			}
            			route.setSteps(steps);
            		}      		            		
            	}
            	routes.add(route);
            }
           
        }
		return routes;
	}
	
	private Step getStepInfo(Element e) {
		Step step =  new Step();
		ArrayList<Point> points = new ArrayList<Point>();
		for (Iterator<Element> i = e.elementIterator(); i.hasNext(); ) {
            Element temp = (Element) i.next();
			if (temp.getName().equals("stepNo")) {
				String stepString = temp.getText();
				Integer stepNo = stepString.length() > 0 ? Integer.parseInt(stepString) : 0;
				step.setStepNo(stepNo);
			}else if (temp.getName().equals("name")){
            	//System.out.println("Name: " + temp.getText());
            	step.setName(temp.getText());
            } else if (temp.getName().equals("task")){
            	//System.out.println("Text: " + temp.getText());
            	step.setTask(temp.getText());
            } else if (temp.getName().equals("treasure")){
            	//System.out.println("treasure: " + temp.getText());
            	step.setTreasure(temp.getText());
            } else if (temp.getName().equals("solution")){
            	//System.out.println("Gesture: " + temp.getText());
            	step.setSolution(temp.getText());
            } else if (temp.getName().equals("points")){   
            	for (Iterator<Element> k = temp.elementIterator(); k.hasNext(); ){
            		Element e2 = (Element) k.next();
            		Point point = getPointInfo(e2);
            		points.add(point);
            		System.out.println(point.toString());
    			}
            	step.setPoints(points);
            } else if (temp.getName().equals("questions")){
				step.setQuestion(getQuestions(temp));
			}
        }
		return step;
	}

	private ArrayList<Question> getQuestions(Element questionsElement) {
		ArrayList<Question> questions = new ArrayList<Question>();

		for (Iterator<Element> questionsIterator = questionsElement.elementIterator(); questionsIterator.hasNext();){
			Element questionElement = (Element) questionsIterator.next();
			Question question = new Question();

			for (Iterator<Element> j = questionElement.elementIterator(); j.hasNext();){
				Element questionDetail = (Element) j.next();
				if (questionDetail.getName().equals("question")) {
					question.setQuestion(questionDetail.getText());
				} else if (questionDetail.getName().equals("isImage")) {
					boolean isImage = questionDetail.getText().equals("true");
					question.setIsImage(false);
				} else if (questionDetail.getName().equals("answers")) {
					ArrayList<String> answers = new ArrayList<String>();
					for (Iterator<Element> ai = questionDetail.elementIterator(); ai.hasNext(); ) {
						Element aElement = ai.next();
						if (aElement.getName().equals("answers")) {
							answers.add(aElement.getText());
						}
					}
					question.setAnswers(answers);
				} else if (questionDetail.getName().equals("correctAnswer")) {
					question.setCorrectAnswer(questionDetail.getText());
				} else if (questionDetail.getName().equals("image")) {
					question.setImageString(questionDetail.getText());
				}
			}
			questions.add(question);
		}

		return questions;
	}
	
	private Point getPointInfo(Element e) {
		Point point =  new Point();
		for (Iterator<Element> i = e.elementIterator(); i.hasNext(); ) {
            Element temp = (Element) i.next();
            if (temp.getName().equals("pointNo")) {
				point.setPointNo(Integer.parseInt(temp.getText()));
			} else if (temp.getName().equals("name")){
            	//System.out.println("Name: " + temp.getText());
            	point.setName(temp.getText());
            } else if (temp.getName().equals("latitude")){
            	//System.out.println("Latitude: " + temp.getText());
            	point.setLatitude(Double.parseDouble(temp.getText()));
            } else if (temp.getName().equals("longitude")){
	            //System.out.println("Latitude: " + temp.getText());
	            point.setLongitude(Double.parseDouble(temp.getText()));
            } else if (temp.getName().equals("hr")){
	            //System.out.println("Latitude: " + temp.getText());
	            point.setHr(getHRInfo(temp));
            }
		}
		return point;
	}
	
	private HeartRate getHRInfo(Element e) {
		HeartRate hr = new HeartRate();
		for (Iterator<Element> i = e.elementIterator(); i.hasNext(); ) {
            Element temp = (Element) i.next();
            if (temp.getName().equals("mRange")){
            	System.out.println("mRange: " + temp.getText());
            	hr.setMRange(Float.parseFloat(temp.getText()));
            } else if (temp.getName().equals("sRange")){
            	System.out.println("sRange: " + temp.getText());
            	hr.setSRange(Float.parseFloat(temp.getText()));
            }
		}
		return hr;
	}
}
